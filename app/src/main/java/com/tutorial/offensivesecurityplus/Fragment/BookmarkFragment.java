package com.tutorial.offensivesecurityplus.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.tutorial.offensivesecurityplus.Adapter.NewsThumbnailAdapter;
import com.tutorial.offensivesecurityplus.Activity.NewsDetailActivity;
import com.tutorial.offensivesecurityplus.R;
import com.tutorial.offensivesecurityplus.Retrofit.APIService;
import com.tutorial.offensivesecurityplus.Retrofit.ApiUtils;
import com.tutorial.offensivesecurityplus.Retrofit.NewsThumbnail;
import com.tutorial.offensivesecurityplus.Retrofit.NewsThumbnailResponse;
import com.tutorial.offensivesecurityplus.util.UserStatus;

import java.util.List;

import retrofit2.Call;

public class BookmarkFragment extends Fragment {

    View view;

    List<NewsThumbnail> list = null;

    private APIService mAPIService;
    private String TAG = BookmarkFragment.class.getSimpleName();

    public BookmarkFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_bookmark, container, false);

        final ListView listView = (ListView) view.findViewById(R.id.listView);

        mAPIService = ApiUtils.getAPIService();
        mAPIService.getBookmarks(UserStatus.getUID()).enqueue(new retrofit2.Callback<NewsThumbnailResponse>() {
            @Override
            public void onResponse(Call<NewsThumbnailResponse> call, retrofit2.Response<NewsThumbnailResponse> response) {
                Log.d(TAG, "post response");
                Log.d(TAG, response.code() + "");
                if (response.isSuccessful()) {
                    Log.d(TAG,response.body()+"");
                    list = response.body().getNewsThumbnail();

                    NewsThumbnailAdapter newsThumbnailAdapter = new NewsThumbnailAdapter(getContext(),getActivity(),"bookmark",list);
                    listView.setAdapter(newsThumbnailAdapter);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent intent = new Intent(getContext(),NewsDetailActivity.class);
                            intent.putExtra("article_id",list.get(position).getArticleId());
                            startActivity(intent);
                        }
                    });
                }
            }
            @Override
            public void onFailure(Call<NewsThumbnailResponse> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API. "+t.getMessage());
            }
        });


        return view;
    }

}
