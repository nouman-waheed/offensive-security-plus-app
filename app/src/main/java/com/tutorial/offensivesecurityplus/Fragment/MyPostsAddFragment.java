package com.tutorial.offensivesecurityplus.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.tutorial.offensivesecurityplus.Activity.NewsDetailActivity;
import com.tutorial.offensivesecurityplus.Adapter.NewsThumbnailAdapter;
import com.tutorial.offensivesecurityplus.R;
import com.tutorial.offensivesecurityplus.Retrofit.APIService;
import com.tutorial.offensivesecurityplus.Retrofit.ApiUtils;
import com.tutorial.offensivesecurityplus.Retrofit.NewsThumbnailResponse;
import com.tutorial.offensivesecurityplus.util.UserStatus;

import retrofit2.Call;


public class MyPostsAddFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private int mParam1;
    private String mParam2;

    private APIService mAPIService;
    private String TAG = MyPostsAddFragment.class.getSimpleName();


    public MyPostsAddFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EmptyFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyPostsAddFragment newInstance(int param1, String param2) {
        MyPostsAddFragment fragment = new MyPostsAddFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getInt(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_myposts_add, container, false);
        final EditText title = (EditText) view.findViewById(R.id.title);
        final EditText description = (EditText) view.findViewById(R.id.description);
        FloatingActionButton add_btn = (FloatingActionButton) view.findViewById(R.id.add_btn);

        add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(UserStatus.isLoggedIn()) {

                    if (title.getText().toString().length() > 0 && description.getText().toString().length() > 0) {

                        mAPIService = ApiUtils.getAPIService();
                        mAPIService.createPost(UserStatus.getUID(), title.getText().toString(), description.getText().toString()).enqueue(new retrofit2.Callback<Integer>() {
                            @Override
                            public void onResponse(Call<Integer> call, retrofit2.Response<Integer> response) {
                                Log.d(TAG, "post response");
                                Log.d(TAG, response.code() + "");
                                if (response.isSuccessful()) {
                                    Log.d(TAG, response.body() + "");
                                    if (response.body() == 1) {

                                        final MyPostFragment myPostFragment = new MyPostFragment();
                                        getActivity().getSupportFragmentManager()
                                                .beginTransaction()
                                                .replace(R.id.frameLayout, myPostFragment)
                                                .addToBackStack(null)
                                                .commit();
                                    } else {
                                        Toast.makeText(getContext(), "Your post were not created.", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<Integer> call, Throwable t) {
                                Log.e(TAG, "Unable to submit post to API. " + t.getMessage());
                            }
                        });

                    } else {
                        Toast.makeText(getContext(), "You must have to enter title and description.", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    Toast.makeText(getContext(), "You must have to signin to create topics.", Toast.LENGTH_SHORT).show();
                }
            }
        });


        return view;
    }

}
