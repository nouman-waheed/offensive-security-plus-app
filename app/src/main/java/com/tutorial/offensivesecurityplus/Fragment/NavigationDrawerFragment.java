package com.tutorial.offensivesecurityplus.Fragment;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.facebook.login.LoginManager;
import com.tutorial.offensivesecurityplus.Activity.AboutActivity;
import com.tutorial.offensivesecurityplus.Adapter.NavigationDrawerAdapter;
import com.tutorial.offensivesecurityplus.Activity.BadgesActivity;
import com.tutorial.offensivesecurityplus.Activity.FeedbackActivity;
import com.tutorial.offensivesecurityplus.Activity.LoginActivity;
import com.tutorial.offensivesecurityplus.Activity.MainActivity;
import com.tutorial.offensivesecurityplus.Model.Config;
import com.tutorial.offensivesecurityplus.Model.Recycler_Items;
import com.tutorial.offensivesecurityplus.R;
import com.tutorial.offensivesecurityplus.Activity.ReputationActivity;
import com.tutorial.offensivesecurityplus.Activity.SettingsActivity;
import com.tutorial.offensivesecurityplus.Activity.SupportActivity;

import java.util.ArrayList;
import java.util.List;

import com.tutorial.offensivesecurityplus.Logic.CircleImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class NavigationDrawerFragment extends Fragment
{
    private RecyclerView recyclerView;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private boolean mUserLearnedDrawer;
    private  boolean mFromSavedInstance;
    public  static final String PREF_FILE_NAME="testpref";
    public static final String KEY_USER_LEARNED_DRAWER="user_learned_drawer";
    private View containerView;
    // recycler adapter
    private NavigationDrawerAdapter adapter;
    private static String[] appBarTitles;

    public NavigationDrawerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserLearnedDrawer= Boolean.valueOf(readFromPreference(getActivity(),KEY_USER_LEARNED_DRAWER,"false"));
        if(savedInstanceState!=null)
        {
            mFromSavedInstance=true;
        }
    }

    /**
     * @param encodedString
     * @return bitmap (from given string)
     */
    public Bitmap StringToBitmap(String encodedString){
        try {
            byte [] encodeByte= Base64.decode(encodedString,Base64.DEFAULT);
            Bitmap bitmap= BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch(Exception e) {
            e.getMessage();
            return null;
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_navigation_drawer,container,false);

        TextView textView = (TextView) layout.findViewById(R.id.headText);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), LoginActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });

        final SharedPreferences pref = getContext().getSharedPreferences(Config.SHARED_PREF, 0);

        if(pref.getString("image",null) != null){
            ((CircleImageView)layout.findViewById(R.id.imageView)).setImageBitmap(StringToBitmap(pref.getString("image",null)));
        }

        if(pref.getString("regId", null) != null){
            ((RelativeLayout) layout.findViewById(R.id.namearea)).setVisibility(View.VISIBLE);
            ((TextView) layout.findViewById(R.id.nameText)).setText(pref.getString("name", null));

            if(pref.getString("source", null).equals("facebook"))
                ((RelativeLayout) layout.findViewById(R.id.facebook)).setVisibility(View.VISIBLE);
            else if(pref.getString("source", null).equals("twitter"))
                ((RelativeLayout) layout.findViewById(R.id.twitter)).setVisibility(View.VISIBLE);
            else if(pref.getString("source", null).equals("google"))
                ((RelativeLayout) layout.findViewById(R.id.google)).setVisibility(View.VISIBLE);


            textView.setText("Logout");
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferences.Editor editor = pref.edit();
                    editor.clear();
                    editor.commit();

                    Toast.makeText(getContext(), "Successfully Logged Out.", Toast.LENGTH_SHORT).show();

                    LoginManager.getInstance().logOut();

                    Intent intent = new Intent(getContext(),MainActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                }
            });
        }



        recyclerView=(RecyclerView)layout.findViewById(R.id.drawerlist);
        adapter=new NavigationDrawerAdapter(getActivity(),getData());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position)
            {
                mDrawerLayout.closeDrawer(containerView);

                if(position == 0){
                    Intent intent = new Intent(getContext(), ReputationActivity.class);
                    startActivity(intent);
                }
                else if(position == 1){
                    Intent intent = new Intent(getContext(), BadgesActivity.class);
                    startActivity(intent);
                }
                else if(position == 4){
                    try {
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("text/plain");
                        i.putExtra(Intent.EXTRA_SUBJECT, "Offensive Security Plus");
                        String sAux = "\nLet me recommend you this application\n\n";
                        sAux = sAux + "https://play.google.com/store/apps/details?id=Orion.Soft \n\n";
                        i.putExtra(Intent.EXTRA_TEXT, sAux);
                        startActivity(Intent.createChooser(i, "Share link:"));
                    } catch(Exception e) {
                        //e.toString();
                    }
                }
                else if(position == 2){
                    Intent intent = new Intent(getContext(), AboutActivity.class);
                    startActivity(intent);
                }
                else if(position == 3){
                    Intent intent = new Intent(getContext(), SettingsActivity.class);
                    startActivity(intent);
                }
                else if(position == 5){
                    Intent intent = new Intent(getContext(), FeedbackActivity.class);
                    startActivity(intent);
                }
                else if(position == 6){
                    Intent intent = new Intent(getContext(), SupportActivity.class);
                    startActivity(intent);
                }

            }


            @Override
            public void onLongClick(View view, int position)
            {
                //Toast.makeText(getActivity(),"on Long click triggered",Toast.LENGTH_SHORT).show();
            }
        }));
        return layout;

    }

    public static List<Recycler_Items> getData()
    {
        List<Recycler_Items> data=new ArrayList<>();
        int[] icons={R.drawable.reputation,R.drawable.badge,R.drawable.about,R.drawable.settings,R.drawable.share,R.drawable.feedback,R.drawable.help,R.drawable.training,R.drawable.course};
        String[] titles={"Reputation","Badges","About","Settings","Share","Feedback","Support","Trainings","Courses"};
        appBarTitles = titles;
        for (int i=0;i < titles.length && i < icons.length;i++)
        {
            Recycler_Items current=new Recycler_Items();
            current.IconId=icons[i];
            current.title=titles[i];
            data.add(current);
        }

        return data;

    }


    public void setUp(int fragmentID, DrawerLayout drawer, final Toolbar toolbar)
    {
        containerView=getActivity().findViewById(fragmentID);
        mDrawerLayout = drawer;
        mDrawerToggle=new ActionBarDrawerToggle(getActivity(),drawer,toolbar,R.string.drawer_open,R.string.drawer_close)
        {
            @Override
            public void onDrawerOpened(View drawerView)
            {
                super.onDrawerOpened(drawerView);
                if(!mUserLearnedDrawer)
                {
                    mUserLearnedDrawer=true;
                    saveToPreference(getActivity(),KEY_USER_LEARNED_DRAWER,mUserLearnedDrawer+"");
                }
                getActivity().invalidateOptionsMenu();
                mDrawerToggle.syncState();
            }

            @Override
            public void onDrawerClosed(View drawerView)
            {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
                mDrawerToggle.syncState();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                if(slideOffset < 0.6)
                {
                    toolbar.setAlpha(1-slideOffset);
                }
            }
        };
        if(!mUserLearnedDrawer && !mFromSavedInstance)
        {
            mDrawerLayout.openDrawer(containerView);
        }


        mDrawerLayout.setDrawerListener(mDrawerToggle);

        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });


    }
    public static void saveToPreference(Context context, String preferenceName, String preferenceValue)
    {
        SharedPreferences preferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(preferenceName,preferenceValue);
        editor.apply();
    }
    public static String readFromPreference(Context context, String preferenceName, String defaultValue)
    {
        SharedPreferences preferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(preferenceName,defaultValue);
    }

//    @Override
//    public void itemclicked(View view, int position)
//    {
//        startActivity(new Intent(getActivity(),Submenu.class));
//
//    }


    class RecyclerTouchListener implements RecyclerView.OnItemTouchListener
    {
        //Gesture detector to detect a click on item
        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        //constructor

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener)
        {
            this.clickListener=clickListener;
            gestureDetector=new GestureDetector(context,new GestureDetector.SimpleOnGestureListener(){
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                  View child =  recyclerView.findChildViewUnder(e.getX(),e.getY());

                  if(child!=null && clickListener!=null)
                  {
                      clickListener.onLongClick(child,recyclerView.getChildAdapterPosition(child));



                  }


                }
            });
        }


        //implemented methods
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child =  rv.findChildViewUnder(e.getX(),e.getY());
            if(child!=null && clickListener!=null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child,rv.getChildAdapterPosition(child));

            }

            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }

    }
    public static interface ClickListener
    {
        public void onClick(View view, int position);
        public void onLongClick(View view, int position);

    }
}
