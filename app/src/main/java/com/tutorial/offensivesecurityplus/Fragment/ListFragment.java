package com.tutorial.offensivesecurityplus.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.tutorial.offensivesecurityplus.Adapter.NewsThumbnailAdapter;
import com.tutorial.offensivesecurityplus.Activity.NewsDetailActivity;
import com.tutorial.offensivesecurityplus.R;
import com.tutorial.offensivesecurityplus.Retrofit.APIService;
import com.tutorial.offensivesecurityplus.Retrofit.ApiUtils;
import com.tutorial.offensivesecurityplus.Retrofit.NewsThumbnail;
import com.tutorial.offensivesecurityplus.Retrofit.NewsThumbnailResponse;
import com.tutorial.offensivesecurityplus.util.UserStatus;

import java.util.List;

import retrofit2.Call;

public class ListFragment extends Fragment {

    LinearLayout new_tab, major_tab, discuss_tab;
    TextView new_text,major_text,discuss_text;

    View view;

    List<NewsThumbnail> list = null;

    private String uid = "0";
    private String topic = "none";

    private APIService mAPIService;
    private String TAG = ListFragment.class.getSimpleName();

    ListView listView;

    public ListFragment() {
        // Required empty public constructor
    }


    private void setViews(){
        new_tab = (LinearLayout) view.findViewById(R.id.new_tab);
        major_tab = (LinearLayout) view.findViewById(R.id.major_tab);
        discuss_tab = (LinearLayout) view.findViewById(R.id.discuss_tab);
        new_text = (TextView) view.findViewById(R.id.new_text);
        major_text = (TextView) view.findViewById(R.id.major_text);
        discuss_text = (TextView) view.findViewById(R.id.discuss_text);

        new_tab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new_tab.setBackgroundColor(getResources().getColor(R.color.nav_blue));
                new_text.setTextColor(getResources().getColor(R.color.white));
                final int sdk = android.os.Build.VERSION.SDK_INT;
                if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    major_tab.setBackgroundDrawable( getResources().getDrawable(R.drawable.border) );
                    discuss_tab.setBackgroundDrawable( getResources().getDrawable(R.drawable.border) );
                } else {
                    major_tab.setBackground( getResources().getDrawable(R.drawable.border));
                    discuss_tab.setBackground( getResources().getDrawable(R.drawable.border));
                }
                major_text.setTextColor(getResources().getColor(R.color.nav_blue));
                discuss_text.setTextColor(getResources().getColor(R.color.nav_blue));

                mAPIService = ApiUtils.getAPIService();
                mAPIService.fetchNewsThumbnail(uid,topic).enqueue(new retrofit2.Callback<NewsThumbnailResponse>() {
                    @Override
                    public void onResponse(Call<NewsThumbnailResponse> call, retrofit2.Response<NewsThumbnailResponse> response) {
                        Log.d(TAG, "post response");
                        Log.d(TAG, response.code() + "");
                        if (response.isSuccessful()) {
                            Log.d(TAG,response.body()+"");
                            list = response.body().getNewsThumbnail();

                            NewsThumbnailAdapter newsThumbnailAdapter = new NewsThumbnailAdapter(getContext(),getActivity(),"list",list);
                            listView.setAdapter(newsThumbnailAdapter);

                            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    Intent intent = new Intent(getContext(),NewsDetailActivity.class);
                                    intent.putExtra("article_id",list.get(position).getArticleId());
                                    startActivity(intent);
                                }
                            });
                        }
                    }
                    @Override
                    public void onFailure(Call<NewsThumbnailResponse> call, Throwable t) {
                        Log.e(TAG, "Unable to submit post to API. "+t.getMessage());
                    }
                });

            }
        });
        major_tab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                major_tab.setBackgroundColor(getResources().getColor(R.color.nav_blue));
                major_text.setTextColor(getResources().getColor(R.color.white));
                final int sdk = android.os.Build.VERSION.SDK_INT;
                if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    new_tab.setBackgroundDrawable( getResources().getDrawable(R.drawable.border) );
                    discuss_tab.setBackgroundDrawable( getResources().getDrawable(R.drawable.border) );
                } else {
                    new_tab.setBackground( getResources().getDrawable(R.drawable.border));
                    discuss_tab.setBackground( getResources().getDrawable(R.drawable.border));
                }
                new_text.setTextColor(getResources().getColor(R.color.nav_blue));
                discuss_text.setTextColor(getResources().getColor(R.color.nav_blue));

                mAPIService = ApiUtils.getAPIService();
                mAPIService.getMajorArticles(uid,topic).enqueue(new retrofit2.Callback<NewsThumbnailResponse>() {
                    @Override
                    public void onResponse(Call<NewsThumbnailResponse> call, retrofit2.Response<NewsThumbnailResponse> response) {
                        Log.d(TAG, "post response");
                        Log.d(TAG, response.code() + "");
                        if (response.isSuccessful()) {
                            Log.d(TAG,response.body()+"");
                            list = response.body().getNewsThumbnail();

                            NewsThumbnailAdapter newsThumbnailAdapter = new NewsThumbnailAdapter(getContext(),getActivity(),"list",list);
                            listView.setAdapter(newsThumbnailAdapter);

                            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    Intent intent = new Intent(getContext(),NewsDetailActivity.class);
                                    intent.putExtra("article_id",list.get(position).getArticleId());
                                    startActivity(intent);
                                }
                            });
                        }
                    }
                    @Override
                    public void onFailure(Call<NewsThumbnailResponse> call, Throwable t) {
                        Log.e(TAG, "Unable to submit post to API. "+t.getMessage());
                    }
                });

            }
        });
        discuss_tab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(UserStatus.isLoggedIn()) {
                    discuss_tab.setBackgroundColor(getResources().getColor(R.color.nav_blue));
                    discuss_text.setTextColor(getResources().getColor(R.color.white));
                    final int sdk = android.os.Build.VERSION.SDK_INT;
                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        major_tab.setBackgroundDrawable(getResources().getDrawable(R.drawable.border));
                        new_tab.setBackgroundDrawable(getResources().getDrawable(R.drawable.border));
                    } else {
                        major_tab.setBackground(getResources().getDrawable(R.drawable.border));
                        new_tab.setBackground(getResources().getDrawable(R.drawable.border));
                    }
                    new_text.setTextColor(getResources().getColor(R.color.nav_blue));
                    major_text.setTextColor(getResources().getColor(R.color.nav_blue));

                    mAPIService = ApiUtils.getAPIService();
                    mAPIService.getDiscussedArticles(uid, topic).enqueue(new retrofit2.Callback<NewsThumbnailResponse>() {
                        @Override
                        public void onResponse(Call<NewsThumbnailResponse> call, retrofit2.Response<NewsThumbnailResponse> response) {
                            Log.d(TAG, "post response");
                            Log.d(TAG, response.code() + "");
                            if (response.isSuccessful()) {
                                Log.d(TAG, response.body() + "");
                                list = response.body().getNewsThumbnail();

                                NewsThumbnailAdapter newsThumbnailAdapter = new NewsThumbnailAdapter(getContext(), getActivity(), "list", list);
                                listView.setAdapter(newsThumbnailAdapter);

                                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        Intent intent = new Intent(getContext(), NewsDetailActivity.class);
                                        intent.putExtra("article_id", list.get(position).getArticleId());
                                        startActivity(intent);
                                    }
                                });
                            }
                        }

                        @Override
                        public void onFailure(Call<NewsThumbnailResponse> call, Throwable t) {
                            Log.e(TAG, "Unable to submit post to API. " + t.getMessage());
                        }
                    });
                }
                else{
                    Toast.makeText(getContext(), "You must have to signin to see discussed articles.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_list, container, false);

        listView = (ListView) view.findViewById(R.id.listView);

        setViews();


        if(UserStatus.isLoggedIn()){
            uid = UserStatus.getUID();
        }

        mAPIService = ApiUtils.getAPIService();
        mAPIService.fetchNewsThumbnail(uid,topic).enqueue(new retrofit2.Callback<NewsThumbnailResponse>() {
            @Override
            public void onResponse(Call<NewsThumbnailResponse> call, retrofit2.Response<NewsThumbnailResponse> response) {
                Log.d(TAG, "post response");
                Log.d(TAG, response.code() + "");
                if (response.isSuccessful()) {
                    Log.d(TAG,response.body()+"");
                    list = response.body().getNewsThumbnail();

                    NewsThumbnailAdapter newsThumbnailAdapter = new NewsThumbnailAdapter(getContext(),getActivity(),"list",list);
                    listView.setAdapter(newsThumbnailAdapter);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent intent = new Intent(getContext(),NewsDetailActivity.class);
                            intent.putExtra("article_id",list.get(position).getArticleId());
                            startActivity(intent);
                        }
                    });
                }
            }
            @Override
            public void onFailure(Call<NewsThumbnailResponse> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API. "+t.getMessage());
            }
        });


        return view;
    }

}
