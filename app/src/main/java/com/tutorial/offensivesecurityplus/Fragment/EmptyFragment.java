package com.tutorial.offensivesecurityplus.Fragment;

import android.content.Context;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.tutorial.offensivesecurityplus.R;
import com.tutorial.offensivesecurityplus.util.UserStatus;


public class EmptyFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private int mParam1;
    private String mParam2;


    public EmptyFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EmptyFragment.
     */
    public static EmptyFragment newInstance(int param1, String param2) {
        EmptyFragment fragment = new EmptyFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getInt(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_empty, container, false);
        ImageView imageView = (ImageView) view.findViewById(R.id.icon);
        imageView.setImageResource(mParam1);
        TextView textView = (TextView) view.findViewById(R.id.details);
        textView.setText(mParam2);

        if(mParam1 == R.drawable.add_button){
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(UserStatus.isLoggedIn()) {
                        final MyPostsAddFragment myPostsAddFragment = new MyPostsAddFragment();
                        getActivity().getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.frameLayout, myPostsAddFragment)
                                .addToBackStack(null)
                                .commit();
                    }
                    else{
                        Toast.makeText(getContext(), "You must have to signin to create topics.", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        return view;
    }

}
