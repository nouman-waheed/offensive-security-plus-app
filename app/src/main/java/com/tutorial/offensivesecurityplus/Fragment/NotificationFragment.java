package com.tutorial.offensivesecurityplus.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.tutorial.offensivesecurityplus.Activity.NewsDetailActivity;
import com.tutorial.offensivesecurityplus.Adapter.NewsThumbnailAdapter;
import com.tutorial.offensivesecurityplus.Adapter.NotificationAdapter;
import com.tutorial.offensivesecurityplus.R;
import com.tutorial.offensivesecurityplus.Retrofit.APIService;
import com.tutorial.offensivesecurityplus.Retrofit.ApiUtils;
import com.tutorial.offensivesecurityplus.Retrofit.NewsThumbnail;
import com.tutorial.offensivesecurityplus.Retrofit.NewsThumbnailResponse;
import com.tutorial.offensivesecurityplus.Retrofit.Notification;
import com.tutorial.offensivesecurityplus.Retrofit.NotificationResponse;
import com.tutorial.offensivesecurityplus.util.UserStatus;

import java.util.List;

import retrofit2.Call;

public class NotificationFragment extends Fragment {

    View view;

    List<Notification> list = null;

    private APIService mAPIService;
    private String TAG = NotificationFragment.class.getSimpleName();

    public NotificationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_notification, container, false);

        final ListView listView = (ListView) view.findViewById(R.id.listView);

        mAPIService = ApiUtils.getAPIService();
        mAPIService.getNotifications(UserStatus.getUID()).enqueue(new retrofit2.Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, retrofit2.Response<NotificationResponse> response) {
                Log.d(TAG, "post response");
                Log.d(TAG, response.code() + "");
                if (response.isSuccessful()) {
                    Log.d(TAG,response.body()+"");
                    list = response.body().getNotifications();

                    NotificationAdapter newsThumbnailAdapter = new NotificationAdapter(getContext(),getActivity(),list);
                    listView.setAdapter(newsThumbnailAdapter);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent intent = new Intent(getContext(),NewsDetailActivity.class);
                            intent.putExtra("article_id",list.get(position).getArticleId());
                            startActivity(intent);
                        }
                    });
                }
            }
            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API. "+t.getMessage());
            }
        });


        return view;
    }

}
