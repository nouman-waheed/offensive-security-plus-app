package com.tutorial.offensivesecurityplus.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.tutorial.offensivesecurityplus.Activity.NewsDetailActivity;
import com.tutorial.offensivesecurityplus.Activity.PostDetailActivity;
import com.tutorial.offensivesecurityplus.Adapter.NewsThumbnailAdapter;
import com.tutorial.offensivesecurityplus.Adapter.PostAdapter;
import com.tutorial.offensivesecurityplus.R;
import com.tutorial.offensivesecurityplus.Retrofit.APIService;
import com.tutorial.offensivesecurityplus.Retrofit.ApiUtils;
import com.tutorial.offensivesecurityplus.Retrofit.NewsThumbnail;
import com.tutorial.offensivesecurityplus.Retrofit.NewsThumbnailResponse;
import com.tutorial.offensivesecurityplus.Retrofit.Post;
import com.tutorial.offensivesecurityplus.Retrofit.PostResponse;
import com.tutorial.offensivesecurityplus.util.UserStatus;

import java.util.List;

import retrofit2.Call;

public class MyPostFragment extends Fragment {

    View view;

    List<Post> list = null;

    private APIService mAPIService;
    private String TAG = MyPostFragment.class.getSimpleName();

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    private String mParam1;

    public MyPostFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment EmptyFragment.
     */
    public static MyPostFragment newInstance(String param1) {
        MyPostFragment fragment = new MyPostFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_mypost, container, false);

        final ListView listView = (ListView) view.findViewById(R.id.listView);
        FloatingActionButton add_btn = (FloatingActionButton) view.findViewById(R.id.add_btn);


        if(mParam1.equals("mypost")){
            mAPIService = ApiUtils.getAPIService();
            mAPIService.getMyPosts(UserStatus.getUID()).enqueue(new retrofit2.Callback<PostResponse>() {
                @Override
                public void onResponse(Call<PostResponse> call, retrofit2.Response<PostResponse> response) {
                    Log.d(TAG, "post response");
                    Log.d(TAG, response.code() + "");
                    if (response.isSuccessful()) {
                        Log.d(TAG,response.body()+"");
                        list = response.body().getPosts();

                        PostAdapter postAdapter = new PostAdapter(getContext(),getActivity(),list);
                        listView.setAdapter(postAdapter);

                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Intent intent = new Intent(getContext(),PostDetailActivity.class);
                                intent.putExtra("community_id",list.get(position).getCommunityId());
                                intent.putExtra("path","");
                                startActivity(intent);
                            }
                        });
                    }
                }
                @Override
                public void onFailure(Call<PostResponse> call, Throwable t) {
                    Log.e(TAG, "Unable to submit post to API. "+t.getMessage());
                }
            });

            add_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final MyPostsAddFragment myPostsAddFragment = new MyPostsAddFragment();
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.frameLayout, myPostsAddFragment)
                            .addToBackStack(null)
                            .commit();
                }
            });
        }
        else{
            add_btn.setVisibility(View.GONE);

            mAPIService = ApiUtils.getAPIService();
            mAPIService.getCommunityPosts(UserStatus.getUID()).enqueue(new retrofit2.Callback<PostResponse>() {
                @Override
                public void onResponse(Call<PostResponse> call, retrofit2.Response<PostResponse> response) {
                    Log.d(TAG, "post response");
                    Log.d(TAG, response.code() + "");
                    if (response.isSuccessful()) {
                        Log.d(TAG,response.body()+"");
                        list = response.body().getPosts();

                        PostAdapter postAdapter = new PostAdapter(getContext(),getActivity(),list);
                        listView.setAdapter(postAdapter);

                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Intent intent = new Intent(getContext(),PostDetailActivity.class);
                                intent.putExtra("community_id",list.get(position).getCommunityId());
                                startActivity(intent);
                            }
                        });
                    }
                }
                @Override
                public void onFailure(Call<PostResponse> call, Throwable t) {
                    Log.e(TAG, "Unable to submit post to API. "+t.getMessage());
                }
            });
        }

        return view;
    }

}
