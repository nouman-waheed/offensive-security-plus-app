package com.tutorial.offensivesecurityplus.util;

import android.content.SharedPreferences;

import com.tutorial.offensivesecurityplus.Model.Config;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by mohammadnuman on 03/07/2017.
 */

public class UserStatus {

    public static Boolean isLoggedIn() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        return (pref.getString("uid",null) != null);
    }

    public static String getUID() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        return pref.getString("uid",null);
    }

    public static String getName() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        return pref.getString("name",null);
    }

    public static String getEmail() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        return pref.getString("email",null);
    }

    public static String getPhotoUrl() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        return pref.getString("image_url",null);
    }

    public static String getPhoto() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        return pref.getString("image",null);
    }

}
