package com.tutorial.offensivesecurityplus.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ListView;

import com.tutorial.offensivesecurityplus.Adapter.BadgesAdapter;
import com.tutorial.offensivesecurityplus.Model.Badge;
import com.tutorial.offensivesecurityplus.R;

import java.util.ArrayList;

public class BadgesActivity extends AppCompatActivity {

    ArrayList<Badge> badges;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_badges);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        badges = new ArrayList<Badge>();
        badges.add(new Badge("Begginer","This badge shows you're the begginer here.",R.drawable.badge_1));
        badges.add(new Badge("Learner","This badge shows you're the learner here.",R.drawable.badge_2));
        badges.add(new Badge("Student","This badge shows you're the student here.",R.drawable.badge_star));
        badges.add(new Badge("Professional","This badge shows you're the professional here.",R.drawable.badge_shield));
        badges.add(new Badge("All Star","This badge shows you've all stars here.",R.drawable.badge_5star));
        badges.add(new Badge("King","This badge shows you're the king here.",R.drawable.badge_crown));

        BadgesAdapter adapter = new BadgesAdapter(this,badges);
        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
