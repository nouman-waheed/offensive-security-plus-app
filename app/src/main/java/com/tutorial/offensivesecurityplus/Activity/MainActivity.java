package com.tutorial.offensivesecurityplus.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tutorial.offensivesecurityplus.Adapter.PostAdapter;
import com.tutorial.offensivesecurityplus.Fragment.BookmarkFragment;
import com.tutorial.offensivesecurityplus.Fragment.EmptyFragment;
import com.tutorial.offensivesecurityplus.Fragment.ListFragment;
import com.tutorial.offensivesecurityplus.Fragment.MyPostFragment;
import com.tutorial.offensivesecurityplus.Fragment.NavigationDrawerFragment;
import com.tutorial.offensivesecurityplus.Fragment.NotificationFragment;
import com.tutorial.offensivesecurityplus.R;
import com.tutorial.offensivesecurityplus.Retrofit.APIService;
import com.tutorial.offensivesecurityplus.Retrofit.ApiUtils;
import com.tutorial.offensivesecurityplus.Retrofit.NewsThumbnailResponse;
import com.tutorial.offensivesecurityplus.Retrofit.Post;
import com.tutorial.offensivesecurityplus.Retrofit.PostResponse;
import com.tutorial.offensivesecurityplus.util.UserStatus;
import com.twitter.sdk.android.core.Twitter;

import java.util.List;

import retrofit2.Call;

public class MainActivity extends AppCompatActivity {

    private Toolbar toolBar;
    public static ActionBar actionBar;
    private LinearLayout nav_news,nav_saveditems,nav_mytopics,nav_community,nav_notifications;
    private ImageView img_news,img_saveditems,img_mytopics,img_community,img_notifications;
    private TextView text_news,text_saveditems,text_mytopics,text_community,text_notifications;

    private APIService mAPIService;
    private String TAG = MainActivity.class.getSimpleName();

    private static String action_nav = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Twitter.initialize(this);

        setViews();

        Toolbar toolBar=(Toolbar)findViewById(R.id.app_bar);
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        actionBar = getSupportActionBar();

        NavigationDrawerFragment navFrag = (NavigationDrawerFragment)getSupportFragmentManager().findFragmentById(R.id.frag_nav_drawer);

        navFrag.setUp(R.id.frag_nav_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolBar);

        setTitle("All News");

        if(action_nav == null) {
            action_nav = "All News";

            final ListFragment listFragment =new ListFragment();
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frameLayout, listFragment)
                    .addToBackStack(null)
                    .commit();
        }
        else{
            if(action_nav.equals("All News")){
                nav_news.performClick();
            }
            else if(action_nav.equals("Saved Items")){
                nav_saveditems.performClick();
            }
            else if(action_nav.equals("Community")){
                nav_community.performClick();
            }
            else if(action_nav.equals("My Topics")){
                nav_mytopics.performClick();
            }
            else if(action_nav.equals("Notifications")){
                nav_notifications.performClick();
            }
        }



    }

    private void setViews(){
        nav_news = (LinearLayout) findViewById(R.id.nav_news);
        nav_saveditems = (LinearLayout) findViewById(R.id.nav_saveditems);
        nav_mytopics = (LinearLayout) findViewById(R.id.nav_mytopics);
        nav_community = (LinearLayout) findViewById(R.id.nav_community);
        nav_notifications = (LinearLayout) findViewById(R.id.nav_notifications);
        img_news = (ImageView) findViewById(R.id.img_news);
        img_saveditems = (ImageView) findViewById(R.id.img_saveditems);
        img_mytopics = (ImageView) findViewById(R.id.img_mytopics);
        img_community = (ImageView) findViewById(R.id.img_community);
        img_notifications = (ImageView) findViewById(R.id.img_notifications);
        text_news = (TextView) findViewById(R.id.text_news);
        text_saveditems = (TextView) findViewById(R.id.text_saveditems);
        text_mytopics = (TextView) findViewById(R.id.text_mytopics);
        text_community = (TextView) findViewById(R.id.text_community);
        text_notifications = (TextView) findViewById(R.id.text_notifications);


        nav_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBottomNav(img_news,text_news);
                final ListFragment listFragment =new ListFragment();
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.frameLayout, listFragment)
                        .addToBackStack(null)
                        .commit();
                actionBar.setTitle("All News");
                action_nav = "All News";
            }
        });

        nav_saveditems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(UserStatus.isLoggedIn()) {
                    mAPIService = ApiUtils.getAPIService();
                    mAPIService.getBookmarks(UserStatus.getUID()).enqueue(new retrofit2.Callback<NewsThumbnailResponse>() {
                        @Override
                        public void onResponse(Call<NewsThumbnailResponse> call, retrofit2.Response<NewsThumbnailResponse> response) {
                            Log.d(TAG, "post response");
                            Log.d(TAG, response.code() + "");
                            if (response.isSuccessful()) {
                                Log.d(TAG, response.body() + "");

                                if (response.body().getNewsThumbnail().size() > 0 && response.body().getNewsThumbnail().get(0).getArticleId() != null) {
                                    setBottomNav(img_saveditems,text_saveditems);

                                    final BookmarkFragment bookmarkFragment = new BookmarkFragment();
                                    getSupportFragmentManager()
                                            .beginTransaction()
                                            .replace(R.id.frameLayout, bookmarkFragment)
                                            .addToBackStack(null)
                                            .commit();
                                    actionBar.setTitle("Saved Items");
                                    action_nav = "Saved Items";

                                }
                                else{
                                    setBottomNav(img_saveditems,text_saveditems);
                                    setEmptyFragment(R.drawable.bookmark,"Saved items will appear here","Saved Items");
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<NewsThumbnailResponse> call, Throwable t) {
                            Log.e(TAG, "Unable to submit post to API. " + t.getMessage());
                        }
                    });
                }
                else{
                    setBottomNav(img_saveditems,text_saveditems);
                    setEmptyFragment(R.drawable.bookmark,"Saved items will appear here","Saved Items");
                }
            }
        });

        nav_mytopics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(UserStatus.isLoggedIn()) {
                    mAPIService = ApiUtils.getAPIService();
                    mAPIService.getMyPosts(UserStatus.getUID()).enqueue(new retrofit2.Callback<PostResponse>() {
                        @Override
                        public void onResponse(Call<PostResponse> call, retrofit2.Response<PostResponse> response) {
                            Log.d(TAG, "post response");
                            Log.d(TAG, response.code() + "");
                            if (response.isSuccessful()) {
                                Log.d(TAG,response.body()+"");
                                if(response.body().getPosts().size() > 0){
                                    setBottomNav(img_mytopics, text_mytopics);
                                    getSupportActionBar().setTitle("My Topics");
                                    action_nav = "My Topics";
                                    final MyPostFragment myPostFragment = MyPostFragment.newInstance("mypost");
                                    getSupportFragmentManager()
                                            .beginTransaction()
                                            .replace(R.id.frameLayout, myPostFragment)
                                            .addToBackStack(null)
                                            .commit();
                                }
                                else{
                                    setBottomNav(img_mytopics, text_mytopics);
                                    setEmptyFragment(R.drawable.add_button, "Click on plus icon to add your topics.", "My Topics");
                                }
                            }
                        }
                        @Override
                        public void onFailure(Call<PostResponse> call, Throwable t) {
                            Log.e(TAG, "Unable to submit post to API. "+t.getMessage());
                        }
                    });
                }
                else{
                    setBottomNav(img_mytopics, text_mytopics);
                    setEmptyFragment(R.drawable.add_button, "Click on plus icon to add your topics.", "My Topics");
                }
            }
        });

        nav_community.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(UserStatus.isLoggedIn()) {
                    mAPIService = ApiUtils.getAPIService();
                    mAPIService.getCommunityPosts(UserStatus.getUID()).enqueue(new retrofit2.Callback<PostResponse>() {
                        @Override
                        public void onResponse(Call<PostResponse> call, retrofit2.Response<PostResponse> response) {
                            Log.d(TAG, "post response");
                            Log.d(TAG, response.code() + "");
                            if (response.isSuccessful()) {
                                Log.d(TAG,response.body()+"");
                                if(response.body().getPosts().size() > 0){
                                    setBottomNav(img_community,text_community);
                                    getSupportActionBar().setTitle("Community");
                                    action_nav = "Community";
                                    final MyPostFragment myPostFragment = MyPostFragment.newInstance("community");
                                    getSupportFragmentManager()
                                            .beginTransaction()
                                            .replace(R.id.frameLayout, myPostFragment)
                                            .addToBackStack(null)
                                            .commit();
                                }
                                else{
                                    setBottomNav(img_community,text_community);
                                    setEmptyFragment(R.drawable.add_button, "Click on plus icon to add your topics.", "My Topics");
                                }
                            }
                        }
                        @Override
                        public void onFailure(Call<PostResponse> call, Throwable t) {
                            Log.e(TAG, "Unable to submit post to API. "+t.getMessage());
                        }
                    });
                }
                else{
                    setBottomNav(img_community,text_community);
                    setEmptyFragment(R.drawable.heartbeat,"Community posts will appear here","Community");
                }
            }
        });

        nav_notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBottomNav(img_notifications,text_notifications);
                final NotificationFragment notificationFragment =new NotificationFragment();
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.frameLayout, notificationFragment)
                        .addToBackStack(null)
                        .commit();
                actionBar.setTitle("Notifications");
                action_nav = "Notifications";
            }
        });
    }

    private void setBottomNav(ImageView img,TextView text){
        img_news.setColorFilter(getResources().getColor(R.color.blue_grey));
        img_saveditems.setColorFilter(getResources().getColor(R.color.blue_grey));
        img_mytopics.setColorFilter(getResources().getColor(R.color.blue_grey));
        img_community.setColorFilter(getResources().getColor(R.color.blue_grey));
        img_notifications.setColorFilter(getResources().getColor(R.color.blue_grey));
        img.setColorFilter(getResources().getColor(R.color.white));

        text_news.setTextColor(getResources().getColor(R.color.blue_grey));
        text_saveditems.setTextColor(getResources().getColor(R.color.blue_grey));
        text_mytopics.setTextColor(getResources().getColor(R.color.blue_grey));
        text_community.setTextColor(getResources().getColor(R.color.blue_grey));
        text_notifications.setTextColor(getResources().getColor(R.color.blue_grey));
        text.setTextColor(getResources().getColor(R.color.white));
    }

    private void setEmptyFragment(int id,String message,String title){
        //replace fragment
        final EmptyFragment emptyFragment = EmptyFragment.newInstance(id,message);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frameLayout, emptyFragment)
                .addToBackStack(null)
                .commit();
        //Set actionbar title
        actionBar.setTitle(title);

        action_nav = title;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(action_nav.equals("All News")){
            nav_news.performClick();
        }
        else if(action_nav.equals("Saved Items")){
            nav_saveditems.performClick();
        }
        else if(action_nav.equals("Community")){
            nav_community.performClick();
        }
        else if(action_nav.equals("My Topics")){
            nav_mytopics.performClick();
        }
        else if(action_nav.equals("Notifications")){
            nav_notifications.performClick();
        }

    }
}
