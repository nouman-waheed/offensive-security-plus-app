package com.tutorial.offensivesecurityplus.Activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;


import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.TwitterAuthProvider;
import com.tutorial.offensivesecurityplus.Model.Config;
import com.tutorial.offensivesecurityplus.Model.Login;
import com.tutorial.offensivesecurityplus.R;
import com.tutorial.offensivesecurityplus.Retrofit.APIService;
import com.tutorial.offensivesecurityplus.Retrofit.ApiUtils;
import com.tutorial.offensivesecurityplus.Retrofit.SigninResponse;
import com.tutorial.offensivesecurityplus.Service.MyFirebaseInstanceIDService;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {


    // UI references.
    private LoginButton loginButton;
    private TwitterLoginButton twitterLoginButton;
    private CallbackManager callbackManager;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private int RC_SIGN_IN = 100;
    private GoogleApiClient mGoogleApiClient;
    private GoogleSignInOptions gso;
    SignInButton signInButton;
    private Login login = new Login();

    private APIService mAPIService;
    private String TAG = LoginActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        ((ImageView)findViewById(R.id.back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });


        FacebookSdk.sdkInitialize(getApplicationContext());

        // Get a shared instance of the FirebaseAuth object//
        mAuth = FirebaseAuth.getInstance();

        // Set up an AuthStateListener that responds to changes in the user's sign-in state//
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                // Retrieve the user’s account data, using the getCurrentUser method//
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // If the user signs in, then display the following message//
                    Log.d(TAG, "onAuthStateChanged" + user.getUid());
                }
            }
        };


        twitterLoginButton = (TwitterLoginButton) findViewById(R.id.twitter_login_button);
        twitterLoginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                // Do something with result, which provides a TwitterSession for making API calls
                Log.d(TAG, "twitterLogin" + result);
                handleTwitterSession(result.data);

                Log.d("Twitter ID",result.data.getId()+"");
                Log.d("Twitter Username",result.data.getUserName()+"");

                login.setSource("twitter");
                login.setId(result.data.getId()+"");
                login.setName(result.data.getUserName());
                login.setImage("https://twitter.com/"+login.getName()+"/profile_image?size=original");

            }

            @Override
            public void failure(TwitterException exception) {
                // Do something on failure
            }
        });

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        signInButton = (SignInButton) findViewById(R.id.sign_in_button);
        signInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        callbackManager = CallbackManager.Factory.create();
        loginButton = (LoginButton)findViewById(R.id.login_button);
        loginButton.setReadPermissions("public_profile");
        loginButton.setReadPermissions("email");


        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("LoginActivity", response.toString());

                                try {
                                    // Application code
                                    String email = object.getString("email");
                                    String name = object.getString("name");
                                    String id = object.getString("id");

                                    Log.d(TAG,id);
                                    Log.d(TAG,name);
                                    Log.d(TAG,email);

                                    login.setSource("facebook");
                                    login.setId(id);
                                    login.setName(name);
                                    login.setEmail(email);
                                    login.setImage("https://graph.facebook.com/" + id + "/picture?type=large");

                                    registerUserWithFCM();
                                }
                                catch (Exception e){
                                    e.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email");
                request.setParameters(parameters);
                request.executeAsync();

                Log.d(TAG,loginResult.getAccessToken().getUserId());

                Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void onCancel() {
                Toast.makeText(LoginActivity.this, "Login attempt canceled.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException e) {
                Toast.makeText(LoginActivity.this, "Login attempt failed.", Toast.LENGTH_SHORT).show();
            }
        });


        ((RelativeLayout)findViewById(R.id.facebook)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                loginButton.performClick();
            }
        });

        ((RelativeLayout)findViewById(R.id.twitter)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                twitterLoginButton.performClick();
            }
        });

        ((RelativeLayout)findViewById(R.id.google)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });


    }




    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
//        FirebaseUser currentUser = mAuth.getCurrentUser();
//        updateUI(currentUser);
    }

    //Exchange the OAuth access token and OAuth secret for a Firebase credential//
    private void handleTwitterSession(TwitterSession session) {
        Log.d(TAG, "handleTwitterSession:" + session);

        AuthCredential credential = TwitterAuthProvider.getCredential(
                session.getAuthToken().token,
                session.getAuthToken().secret);

        //If the call to signInWithCredential succeeds, then get the user’s account data//
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential" + task.isSuccessful());

                        final TwitterSession session = TwitterCore.getInstance().getSessionManager().getActiveSession();
                        TwitterAuthToken authToken = session.getAuthToken();
                        String token = authToken.token;
                        String secret = authToken.secret;

                        final TwitterAuthClient authClient = new TwitterAuthClient();


                        authClient.requestEmail(session, new Callback<String>() {
                            @Override
                            public void success(Result<String> result) {
                                // Do something with the result, which provides the email address
                                Log.d(TAG,"Email = "+result.data);
                                login.setEmail(result.data);
                                registerUserWithFCM();
                            }

                            @Override
                            public void failure(TwitterException exception) {
                                // Do something on failure
                                registerUserWithFCM();
                            }
                        });



                    }
                });
    }

    public static Bitmap getProfilePicture(String url){
        Bitmap bitmap = null;
        try {
            URL imageURL = new URL(url);
            bitmap = BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return bitmap;
    }

    public String BitmapToString(Bitmap bitmap){
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
        byte [] b=baos.toByteArray();
        String temp= Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    private void registerUserWithFCM(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        final SharedPreferences.Editor editor = pref.edit();
        String regId = null;
        if(pref.getString("regId", null) == null) {
            MyFirebaseInstanceIDService service = new MyFirebaseInstanceIDService(this);
            service.onTokenRefresh();
        }
        regId = pref.getString("regId", null);
        login.setToken(regId);

        mAPIService = ApiUtils.getAPIService();
        mAPIService.signinUser(login.getToken(),login.getEmail(),login.getId(),login.getName(),login.getSource(),login.getImage()).enqueue(new retrofit2.Callback<SigninResponse>() {
            @Override
            public void onResponse(Call<SigninResponse> call, retrofit2.Response<SigninResponse> response) {
                Log.d(TAG, "post response");
                Log.d(TAG, response.code() + "");
                if (response.isSuccessful()) {
                    Log.d(TAG,response.body()+"");

                    if(response.body().getError().equals("true")) {
                        Toast.makeText(LoginActivity.this,response.body().getMessage(),Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(LoginActivity.this, "Successfully Authenticated!", Toast.LENGTH_SHORT).show();

                        try{
                            editor.putString("image",BitmapToString(getProfilePicture(response.body().getImage())));
                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }

                        editor.putString("uid",login.getId());
                        editor.putString("name",login.getName());
                        editor.putString("email",login.getEmail());
                        editor.putString("image_url",login.getImage());
                        editor.putString("source",login.getSource());
                        editor.commit();

                        Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
            }
            @Override
            public void onFailure(Call<SigninResponse> call, Throwable t) {

                Log.e(TAG, "Unable to submit post to API. "+t.getMessage());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        twitterLoginButton.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }

    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

//            Log.d(TAG,acct.getEmail().toString());
//
//            Log.d(TAG, acct.getId().toString());
//
//            Log.d(TAG, "Display = "+acct.getDisplayName().toString());
//            Log.d(TAG, "Given = "+acct.getGivenName().toString());
//            Log.d(TAG, "Family = "+acct.getFamilyName().toString());
//
//            Log.d(TAG, acct.getPhotoUrl().toString());

            login.setId(acct.getId().toString());
            login.setName(acct.getDisplayName());
            login.setEmail(acct.getEmail().toString());
            login.setSource("google");
            login.setImage(acct.getPhotoUrl().toString());

            registerUserWithFCM();

        } else {
            // Signed out, show unauthenticated UI.
        }
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(LoginActivity.this,MainActivity.class);
        startActivity(intent);
        finish();
    }
}

