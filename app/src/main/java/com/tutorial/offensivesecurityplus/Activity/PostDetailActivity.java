package com.tutorial.offensivesecurityplus.Activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.tutorial.offensivesecurityplus.Adapter.CommentAdapter;
import com.tutorial.offensivesecurityplus.AsyncTask.ImageDownloaderTask;
import com.tutorial.offensivesecurityplus.R;
import com.tutorial.offensivesecurityplus.Retrofit.APIService;
import com.tutorial.offensivesecurityplus.Retrofit.ApiUtils;
import com.tutorial.offensivesecurityplus.Retrofit.Comment;
import com.tutorial.offensivesecurityplus.Retrofit.NewsDetailsResponse;
import com.tutorial.offensivesecurityplus.Retrofit.PostDetailsResponse;
import com.tutorial.offensivesecurityplus.util.UserStatus;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;

public class PostDetailActivity extends AppCompatActivity {

    private APIService mAPIService;
    private String TAG = PostDetailActivity.class.getSimpleName();
    private PostDetailsResponse details;
    private ImageView main_image,user,share,postcomment,bookmark;
    private TextView timestamp,title,description,comment_count,signin,likes_count;
    private EditText comment_box;
    private List<Comment> list;
    private LinearLayout note;
    public static String notify_uid = "";
    private CommentAdapter commentAdapter = null;
    private static Activity activity = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_detail);
        setViews();

        activity = this;

        final ListView listView = (ListView) findViewById(R.id.listView);

        String uid = "0";

        if(UserStatus.isLoggedIn()){
            uid = UserStatus.getUID();
        }
        mAPIService = ApiUtils.getAPIService();
        mAPIService.postDetails(uid,getIntent().getStringExtra("community_id")).enqueue(new retrofit2.Callback<PostDetailsResponse>() {
            @Override
            public void onResponse(Call<PostDetailsResponse> call, retrofit2.Response<PostDetailsResponse> response) {
                Log.d(TAG, "post response");
                Log.d(TAG, response.code() + "");
                if (response.isSuccessful()) {
                    Log.d(TAG,response.body()+"");
                    details = response.body();

//                    if (main_image != null) {
//                        new ImageDownloaderTask(main_image).execute(details.getImage());
//                    }

//                    main_image.setImageBitmap(getPicture(details.getImage()));

                    timestamp.setText("Posted "+calculateTime(details.getDatestamp())+" ago");
                    title.setText(details.getTitle());
                    description.setText(details.getDescription());
                    comment_count.setText(details.getComments().size()+"");
                    likes_count.setText(details.getLikes()+"");

                    signin.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(PostDetailActivity.this,LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    });

                    postcomment.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(comment_box.getText().toString().length() == 0){
                                Toast.makeText(PostDetailActivity.this, "You cannot post an empty comment.", Toast.LENGTH_SHORT).show();
                            }
                            else{
                                final String reply = comment_box.getText().toString();
                                mAPIService = ApiUtils.getAPIService();
                                mAPIService.postCommunityComment(UserStatus.getUID(),details.getCommunityId(),reply,notify_uid).enqueue(new retrofit2.Callback<Integer>() {
                                    @Override
                                    public void onResponse(Call<Integer> call, retrofit2.Response<Integer> response) {
                                        Log.d(TAG, "post response");
                                        Log.d(TAG, response.code() + "");
                                        if (response.isSuccessful()) {
                                            Log.d(TAG,response.body()+"");

                                            Comment cmnt = new Comment();
                                            cmnt.setReply(reply);
                                            cmnt.setDatestamp("few seconds ago");
                                            cmnt.setLikes("0");
                                            cmnt.setDislikes("0");
                                            cmnt.setCommentId(response.body()+"");
                                            cmnt.setName(UserStatus.getName());
                                            cmnt.setImage(UserStatus.getPhotoUrl());
                                            cmnt.setReaction("none");
                                            cmnt.setUid(UserStatus.getUID());

                                            comment_count.setText(String.valueOf(details.getComments().size()+1));

                                            list.add(0,cmnt);
                                            commentAdapter.notifyDataSetChanged();
                                            setListViewHeightBasedOnChildren(listView);

                                            if(response.body() > 0) {
                                                Toast.makeText(PostDetailActivity.this,"Comment has been posted successfully.",Toast.LENGTH_SHORT).show();
                                                comment_box.setText("");
                                            }
                                            else if(response.body() == 0){
                                                Toast.makeText(PostDetailActivity.this,"Comment has not been posted.",Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    }
                                    @Override
                                    public void onFailure(Call<Integer> call, Throwable t) {
                                        Log.e(TAG, "Unable to submit post to API. "+t.getMessage());
                                    }
                                });
                            }
                        }
                    });

                    share.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(UserStatus.isLoggedIn()){
                                Toast.makeText(PostDetailActivity.this, "This feature will be available soon.", Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Toast.makeText(PostDetailActivity.this, "You must have to login to share post.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                    if(details.getBookmark().equals("true")){
                        bookmark.setImageResource(R.drawable.loved);

                        bookmark.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        switch (which){
                                            case DialogInterface.BUTTON_POSITIVE:
                                                //Yes button clicked
                                                mAPIService = ApiUtils.getAPIService();
                                                mAPIService.unlove(UserStatus.getUID(),details.getCommunityId()).enqueue(new retrofit2.Callback<Integer>() {
                                                    @Override
                                                    public void onResponse(Call<Integer> call, retrofit2.Response<Integer> response) {
                                                        Log.d(TAG, "post response");
                                                        Log.d(TAG, response.code() + "");
                                                        if (response.isSuccessful()) {
                                                            Log.d(TAG,response.body()+"");

                                                            if(response.body() == 1) {
                                                                bookmark.setImageResource(R.drawable.love);
                                                                likes_count.setText(String.valueOf(Integer.parseInt(likes_count.getText().toString())-1));
                                                                Toast.makeText(PostDetailActivity.this,"Post has been removed from liked list successfully.",Toast.LENGTH_SHORT).show();
                                                            }
                                                            else if(response.body() == 0){
                                                                Toast.makeText(PostDetailActivity.this,"Post has not been removed from liked list.",Toast.LENGTH_SHORT).show();
                                                            }
                                                        }
                                                    }
                                                    @Override
                                                    public void onFailure(Call<Integer> call, Throwable t) {
                                                        Log.e(TAG, "Unable to submit post to API. "+t.getMessage());
                                                    }
                                                });

                                                break;

                                            case DialogInterface.BUTTON_NEGATIVE:
                                                //No button clicked
                                                break;
                                        }
                                    }
                                };

                                AlertDialog.Builder builder = new AlertDialog.Builder(PostDetailActivity.this);
                                builder.setMessage("Are you sure! you want to unlike post?").setPositiveButton("Yes", dialogClickListener)
                                        .setNegativeButton("No", dialogClickListener).show();

                            }
                        });
                    }
                    else{
                        bookmark.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(UserStatus.isLoggedIn()){
                                    if(UserStatus.getUID().equals(details.getUid())){
                                        Toast.makeText(PostDetailActivity.this, "You cannot like your own post.", Toast.LENGTH_SHORT).show();
                                    }
                                    else {
                                        mAPIService = ApiUtils.getAPIService();
                                        mAPIService.love(UserStatus.getUID(), details.getCommunityId()).enqueue(new retrofit2.Callback<Integer>() {
                                            @Override
                                            public void onResponse(Call<Integer> call, retrofit2.Response<Integer> response) {
                                                Log.d(TAG, "post response");
                                                Log.d(TAG, response.code() + "");
                                                if (response.isSuccessful()) {
                                                    Log.d(TAG, response.body() + "");

                                                    if (response.body() == 1) {
                                                        likes_count.setText(String.valueOf(Integer.parseInt(likes_count.getText().toString())+1));
                                                        bookmark.setImageResource(R.drawable.loved);
                                                        Toast.makeText(PostDetailActivity.this, "Post has been liked successfully.", Toast.LENGTH_SHORT).show();
                                                    } else if (response.body() == 0) {
                                                        Toast.makeText(PostDetailActivity.this, "Post has not been liked.", Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<Integer> call, Throwable t) {
                                                Log.e(TAG, "Unable to submit post to API. " + t.getMessage());
                                            }
                                        });
                                    }
                                }
                                else{
                                    Toast.makeText(PostDetailActivity.this, "You must have to login to bookmark post.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }


                    user.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(PostDetailActivity.this, "This feature will be coming soon.", Toast.LENGTH_SHORT).show();
                        }
                    });


                    list = details.getComments();

                    if(UserStatus.isLoggedIn()) {
                        commentAdapter = new CommentAdapter(PostDetailActivity.this,PostDetailActivity.this,comment_box,listView, list);

                        listView.setAdapter(commentAdapter);
                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            }
                        });

                        setListViewHeightBasedOnChildren(listView);


                        ((ScrollView) activity.findViewById(R.id.scrollView)).smoothScrollTo(0,0);
                    }
                    else{
                        comment_box.setVisibility(View.GONE);
                        postcomment.setVisibility(View.GONE);
                        note.setVisibility(View.VISIBLE);
                    }

                }
            }
            @Override
            public void onFailure(Call<PostDetailsResponse> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API. "+t.getMessage());
            }
        });


        ((ImageView)findViewById(R.id.back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    public static Bitmap getPicture(String url){
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Bitmap bitmap = null;
        try {
            URL imageURL = new URL(url);
            bitmap = BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return bitmap;
    }

    private String calculateTime(String sql_date){

        //Create a date formatter.
        SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String parseTime = sql_date;
        Date parseDate = null;
        try {
            parseDate = formatter.parse(parseTime);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        Calendar sqlDate = Calendar.getInstance();
        sqlDate.setTime(parseDate);

//        Log.d("Current",sqlDate.get(Calendar.HOUR)+"");
//        Log.d("Current",sqlDate.get(Calendar.MINUTE)+"");
//        Log.d("Current",sqlDate.get(Calendar.SECOND)+"");
//        Log.d("Current",sqlDate.get(Calendar.DATE)+"");
//        Log.d("Current",sqlDate.get(Calendar.MONTH)+"");
//        Log.d("Current",sqlDate.get(Calendar.YEAR)+"");

        Calendar curDate = Calendar.getInstance();
        curDate.setTime(new Date());

//        Log.d("Current",curDate.get(Calendar.HOUR)+"");
//        Log.d("Current",curDate.get(Calendar.MINUTE)+"");
//        Log.d("Current",curDate.get(Calendar.SECOND)+"");
//        Log.d("Current",curDate.get(Calendar.DATE)+"");
//        Log.d("Current",curDate.get(Calendar.MONTH)+"");
//        Log.d("Current",curDate.get(Calendar.YEAR)+"");


        long diffInMillisec = curDate.getTimeInMillis() - sqlDate.getTimeInMillis();

//        long diffInMillisec = sqlDate.getTime() - curr_date.getTime();
        long diffInSec = TimeUnit.MILLISECONDS.toSeconds(diffInMillisec);
        Long seconds = diffInSec % 60;
        diffInSec /= 60;
        Long minutes =diffInSec % 60;
        diffInSec /= 60;
        Long hours = diffInSec % 24;
        diffInSec /= 24;
        Long days = diffInSec;


        if(days > 0)
            return days+"d";
        else if(hours > 0)
            return hours+"h "+minutes+"m";
        else if(minutes > 0)
            return minutes+"m";
        else
            return seconds+"s";
    }


    private void setViews(){
        main_image = (ImageView) findViewById(R.id.main_image);
        user = (ImageView) findViewById(R.id.user);
        share = (ImageView) findViewById(R.id.share);
        postcomment = (ImageView) findViewById(R.id.postcomment);
        bookmark = (ImageView) findViewById(R.id.bookmark);
        timestamp = (TextView) findViewById(R.id.timestamp);
        title = (TextView) findViewById(R.id.title);
        description = (TextView) findViewById(R.id.description);
        comment_count = (TextView) findViewById(R.id.comment_count);
        likes_count = (TextView) findViewById(R.id.likes_count);
        comment_box = (EditText) findViewById(R.id.comment_box);
        note = (LinearLayout) findViewById(R.id.note);
        signin = (TextView) findViewById(R.id.signin);
    }

    public static void setListViewHeightBasedOnChildren(ListView listView)
    {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight=0;
        View view = null;

        for (int i = 0; i < listAdapter.getCount(); i++)
        {
            view = listAdapter.getView(i, view, listView);

            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth,
                        AbsListView.LayoutParams.MATCH_PARENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
            totalHeight += 150;
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + ((listView.getDividerHeight()) * (listAdapter.getCount()));

        listView.setLayoutParams(params);
        listView.setScrollContainer(false);
        listView.requestLayout();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
