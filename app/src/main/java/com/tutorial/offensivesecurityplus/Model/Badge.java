package com.tutorial.offensivesecurityplus.Model;

/**
 * Created by mohammadnuman on 23/06/2017.
 */

public class Badge {
    String title,description;
    int icon;

    public Badge(String title, String description, int icon) {
        this.title = title;
        this.description = description;
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
