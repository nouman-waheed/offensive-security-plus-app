package com.tutorial.offensivesecurityplus.Retrofit;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface APIService {

    @POST("/temp/osp/signin.php")
    @FormUrlEncoded
    Call<SigninResponse> signinUser(@Field("token") String token,
                                    @Field("email") String email,
                                    @Field("uid") String uid,
                                    @Field("name") String name,
                                    @Field("source") String source,
                                    @Field("image") String image);

    @POST("/temp/osp/getAllArticlesThumbnail.php")
    @FormUrlEncoded
    Call<NewsThumbnailResponse> fetchNewsThumbnail(@Field("uid") String uid,
                                                   @Field("topic") String topic);

    @POST("/temp/osp/getMajorArticles.php")
    @FormUrlEncoded
    Call<NewsThumbnailResponse> getMajorArticles(@Field("uid") String uid,
                                                   @Field("topic") String topic);

    @POST("/temp/osp/getDiscussedArticles.php")
    @FormUrlEncoded
    Call<NewsThumbnailResponse> getDiscussedArticles(@Field("uid") String uid,
                                                 @Field("topic") String topic);

    @POST("/temp/osp/bookmark.php")
    @FormUrlEncoded
    Call<Integer> bookmark(@Field("uid") String uid,
                                    @Field("article_id") String article_id);

    @POST("/temp/osp/unbookmark.php")
    @FormUrlEncoded
    Call<Integer> unbookmark(@Field("uid") String uid,
                           @Field("article_id") String article_id);

    @POST("/temp/osp/love.php")
    @FormUrlEncoded
    Call<Integer> love(@Field("uid") String uid,
                           @Field("community_id") String community_id);

    @POST("/temp/osp/unlove.php")
    @FormUrlEncoded
    Call<Integer> unlove(@Field("uid") String uid,
                             @Field("community_id") String community_id);

    @POST("/temp/osp/newsDetails.php")
    @FormUrlEncoded
    Call<NewsDetailsResponse> newsDetails(@Field("uid") String uid,
                                          @Field("article_id") String article_id);

    @POST("/temp/osp/getBookmarks.php")
    @FormUrlEncoded
    Call<NewsThumbnailResponse> getBookmarks(@Field("uid") String uid);

    @POST("/temp/osp/deleteComment.php")
    @FormUrlEncoded
    Call<Integer> deleteComment(@Field("comment_id") String comment_id);

    @POST("/temp/osp/editComment.php")
    @FormUrlEncoded
    Call<Integer> editComment(@Field("comment_id") String comment_id,
                                @Field("reply") String reply);

    @POST("/temp/osp/postComment.php")
    @FormUrlEncoded
    Call<Integer> postComment(@Field("uid") String uid,
                              @Field("article_id") String article_id,
                              @Field("reply") String reply,
                              @Field("notify_uid") String notify_uid);

    @POST("/temp/osp/postCommunityComment.php")
    @FormUrlEncoded
    Call<Integer> postCommunityComment(@Field("uid") String uid,
                              @Field("community_id") String community_id,
                              @Field("reply") String reply,
                              @Field("notify_uid") String notify_uid);

    @POST("/temp/osp/likeComment.php")
    @FormUrlEncoded
    Call<Integer> likeComment(@Field("uid") String uid,
                           @Field("comment_id") String comment_id);

    @POST("/temp/osp/dislikeComment.php")
    @FormUrlEncoded
    Call<Integer> dislikeComment(@Field("uid") String uid,
                           @Field("comment_id") String comment_id);

    @POST("/temp/osp/unlikeComment.php")
    @FormUrlEncoded
    Call<Integer> unlikeComment(@Field("uid") String uid,
                              @Field("comment_id") String comment_id);

    @POST("/temp/osp/undislikeComment.php")
    @FormUrlEncoded
    Call<Integer> undislikeComment(@Field("uid") String uid,
                                 @Field("comment_id") String comment_id);

    @POST("/temp/osp/unlikeDislikeComment.php")
    @FormUrlEncoded
    Call<Integer> unlikeDislikeComment(@Field("uid") String uid,
                                @Field("comment_id") String comment_id);

    @POST("/temp/osp/undislikeLikeComment.php")
    @FormUrlEncoded
    Call<Integer> undislikeLikeComment(@Field("uid") String uid,
                                   @Field("comment_id") String comment_id);

    @POST("/temp/osp/getNotifications.php")
    @FormUrlEncoded
    Call<NotificationResponse> getNotifications(@Field("uid") String uid);

    @POST("/temp/osp/createPost.php")
    @FormUrlEncoded
    Call<Integer> createPost(@Field("uid") String uid,
                             @Field("title") String title,
                             @Field("description") String description);

    @POST("/temp/osp/getMyPosts.php")
    @FormUrlEncoded
    Call<PostResponse> getMyPosts(@Field("uid") String uid);

    @POST("/temp/osp/getCommunityPosts.php")
    @FormUrlEncoded
    Call<PostResponse> getCommunityPosts(@Field("uid") String uid);

    @POST("/temp/osp/postDetails.php")
    @FormUrlEncoded
    Call<PostDetailsResponse> postDetails(@Field("uid") String uid,
                                          @Field("community_id") String community_id);

}
