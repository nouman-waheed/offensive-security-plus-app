package com.tutorial.offensivesecurityplus.Retrofit;

/**
 * Created by mohammadnuman on 04/07/2017.
 */

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationResponse implements Serializable
{

    @SerializedName("notifications")
    @Expose
    private List<Notification> notifications = null;
    private final static long serialVersionUID = 6730838545038838087L;

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }

}
