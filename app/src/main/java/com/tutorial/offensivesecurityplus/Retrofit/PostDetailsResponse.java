package com.tutorial.offensivesecurityplus.Retrofit;

/**
 * Created by mohammadnuman on 05/07/2017.
 */

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostDetailsResponse implements Serializable
{

    @SerializedName("community_id")
    @Expose
    private String communityId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("datestamp")
    @Expose
    private String datestamp;
    @SerializedName("comments")
    @Expose
    private List<Comment> comments = null;
    @SerializedName("bookmark")
    @Expose
    private String bookmark;
    @SerializedName("uid")
    @Expose
    private String uid;
    @SerializedName("likes")
    @Expose
    private String likes;
    private final static long serialVersionUID = 4819397959904316439L;

    public String getCommunityId() {
        return communityId;
    }

    public void setCommunityId(String communityId) {
        this.communityId = communityId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDatestamp() {
        return datestamp;
    }

    public void setDatestamp(String datestamp) {
        this.datestamp = datestamp;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public String getBookmark() {
        return bookmark;
    }

    public void setBookmark(String bookmark) {
        this.bookmark = bookmark;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }
}