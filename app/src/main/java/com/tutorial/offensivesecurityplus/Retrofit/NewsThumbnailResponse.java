package com.tutorial.offensivesecurityplus.Retrofit;

/**
 * Created by mohammadnuman on 03/07/2017.
 */

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewsThumbnailResponse implements Serializable
{

    @SerializedName("news_thumbnail")
    @Expose
    private List<NewsThumbnail> newsThumbnail = null;
    private final static long serialVersionUID = 3850199397996438729L;

    public List<NewsThumbnail> getNewsThumbnail() {
        return newsThumbnail;
    }

    public void setNewsThumbnail(List<NewsThumbnail> newsThumbnail) {
        this.newsThumbnail = newsThumbnail;
    }

}