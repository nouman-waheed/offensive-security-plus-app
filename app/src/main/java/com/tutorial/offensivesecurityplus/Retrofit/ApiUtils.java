package com.tutorial.offensivesecurityplus.Retrofit;

public class ApiUtils {
    private ApiUtils() {}

    public static final String BASE_URL = "http://192.168.10.2/";

    public static APIService getAPIService() {

        return RetrofitClient.getClient(BASE_URL).create(APIService.class);
    }
}
