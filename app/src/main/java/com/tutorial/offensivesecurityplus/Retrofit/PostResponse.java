package com.tutorial.offensivesecurityplus.Retrofit;

/**
 * Created by mohammadnuman on 05/07/2017.
 */

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostResponse implements Serializable
{

    @SerializedName("posts")
    @Expose
    private List<Post> posts = null;
    private final static long serialVersionUID = 3052096879405531992L;

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

}