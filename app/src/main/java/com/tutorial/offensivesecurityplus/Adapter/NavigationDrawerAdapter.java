package com.tutorial.offensivesecurityplus.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.tutorial.offensivesecurityplus.Model.Recycler_Items;
import com.tutorial.offensivesecurityplus.R;

import java.util.Collections;
import java.util.List;


public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.MyViewHolder>
{
    List<Recycler_Items> data = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;
   // private ClickListener clickListener;

    public NavigationDrawerAdapter(Context context, List<Recycler_Items> data)
    {
        this.context=context;
        inflater= LayoutInflater.from(context);
        this.data=data;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = inflater.inflate(R.layout.custom_row, parent, false);
        MyViewHolder holder=new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position)
    {
        Recycler_Items current=data.get(position);
        holder.title.setText(current.title);
        holder.icon.setImageResource(current.IconId);
    }


    @Override
    public int getItemCount()
    {
        return data.size();
    }

    public void deleteItem(int position)
    {
        data.remove(position);
        notifyItemRemoved(position);
    }
    //setting click listener for recycler items
//    public void setClickedListener(ClickListener clickListener)
//    {
//        this.clickListener=clickListener;
//    }



    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        TextView title;
        ImageView icon;

        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            title= (TextView) itemView.findViewById(R.id.listText);
            icon=(ImageView)itemView.findViewById(R.id.listIcon);
        }


        @Override
        public void onClick(View v)
        {
//            context.startActivity(new Intent(context,Submenu.class));
//            if(clickListener!=null)
//            {
//                clickListener.itemclicked(v,getAdapterPosition());
//            }
        }
    }
//    public  interface ClickListener
//    {
//        public void itemclicked(View view,int position);
//    }
}
