package com.tutorial.offensivesecurityplus.Adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.tutorial.offensivesecurityplus.Model.Badge;
import com.tutorial.offensivesecurityplus.R;

import java.util.ArrayList;

public class BadgesAdapter extends BaseAdapter {
    ArrayList<Badge> data;
    Context context;

    public BadgesAdapter(Context context, ArrayList<Badge> data){
        this.context = context;
        this.data = data;
    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.badge_view,parent,false);

        ((ImageView) view.findViewById(R.id.icon)).setImageResource(data.get(position).getIcon());
        ((TextView) view.findViewById(R.id.title)).setText(data.get(position).getTitle());
        ((TextView) view.findViewById(R.id.description)).setText(data.get(position).getDescription());


        return view;
    }
}
