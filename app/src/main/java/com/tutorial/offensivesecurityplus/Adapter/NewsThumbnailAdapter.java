package com.tutorial.offensivesecurityplus.Adapter;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tutorial.offensivesecurityplus.Activity.NewsDetailActivity;
import com.tutorial.offensivesecurityplus.Fragment.BookmarkFragment;
import com.tutorial.offensivesecurityplus.R;
import com.tutorial.offensivesecurityplus.Retrofit.APIService;
import com.tutorial.offensivesecurityplus.Retrofit.ApiUtils;
import com.tutorial.offensivesecurityplus.Retrofit.NewsThumbnail;
import com.tutorial.offensivesecurityplus.util.UserStatus;
import com.tutorial.offensivesecurityplus.AsyncTask.*;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;

public class NewsThumbnailAdapter extends BaseAdapter {
    List<NewsThumbnail> data;
    Context context;
    Activity activity;
    String fragment;

    private APIService mAPIService;
    private String TAG = NewsThumbnailAdapter.class.getSimpleName();

    public NewsThumbnailAdapter(Context context, Activity activity, List<NewsThumbnail> data){
        this.context = context;
        this.activity = activity;
        this.data = data;
        this.fragment = "";
    }

    public NewsThumbnailAdapter(Context context, Activity activity, String fragment,List<NewsThumbnail> data){
        this.context = context;
        this.activity = activity;
        this.data = data;
        this.fragment = fragment;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    private String calculateTime(String sql_date){

        //Create a date formatter.
        SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String parseTime = sql_date;
        Date parseDate = null;
        try {
            parseDate = formatter.parse(parseTime);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        Calendar sqlDate = Calendar.getInstance();
        sqlDate.setTime(parseDate);

//        Log.d("Current",sqlDate.get(Calendar.HOUR)+"");
//        Log.d("Current",sqlDate.get(Calendar.MINUTE)+"");
//        Log.d("Current",sqlDate.get(Calendar.SECOND)+"");
//        Log.d("Current",sqlDate.get(Calendar.DATE)+"");
//        Log.d("Current",sqlDate.get(Calendar.MONTH)+"");
//        Log.d("Current",sqlDate.get(Calendar.YEAR)+"");

        Calendar curDate = Calendar.getInstance();
        curDate.setTime(new Date());

//        Log.d("Current",curDate.get(Calendar.HOUR)+"");
//        Log.d("Current",curDate.get(Calendar.MINUTE)+"");
//        Log.d("Current",curDate.get(Calendar.SECOND)+"");
//        Log.d("Current",curDate.get(Calendar.DATE)+"");
//        Log.d("Current",curDate.get(Calendar.MONTH)+"");
//        Log.d("Current",curDate.get(Calendar.YEAR)+"");


        long diffInMillisec = curDate.getTimeInMillis() - sqlDate.getTimeInMillis();

//        long diffInMillisec = sqlDate.getTime() - curr_date.getTime();
        long diffInSec = TimeUnit.MILLISECONDS.toSeconds(diffInMillisec);
        Long seconds = diffInSec % 60;
        diffInSec /= 60;
        Long minutes =diffInSec % 60;
        diffInSec /= 60;
        Long hours = diffInSec % 24;
        diffInSec /= 24;
        Long days = diffInSec;


        if(days > 0)
            return days+"d";
        else if(hours > 0)
            return hours+"h "+minutes+"m";
        else if(minutes > 0)
            return minutes+"m";
        else
            return seconds+"s";
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.news_item,parent,false);

        ImageView imageView = (ImageView) view.findViewById(R.id.imageView);

        if (imageView != null) {
            new ImageDownloaderTask(imageView).execute(data.get(position).getImage());
        }

        TextView rating = (TextView) view.findViewById(R.id.rating);
        rating.setText(data.get(position).getRating());

        TextView tag = (TextView) view.findViewById(R.id.tag);
        tag.setText(data.get(position).getTag());

        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(data.get(position).getTitle().substring(0,50)+"...");

        TextView description = (TextView) view.findViewById(R.id.description);
        description.setText(data.get(position).getDescription().substring(0,100)+"...");

        TextView timestamp = (TextView) view.findViewById(R.id.timestamp);
        timestamp.setText("Posted "+calculateTime(data.get(position).getDatestamp())+" ago");

        TextView comment_count = (TextView) view.findViewById(R.id.comment_count);
        comment_count.setText(data.get(position).getComments());


        ImageView share = (ImageView) view.findViewById(R.id.share);
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(UserStatus.isLoggedIn()){
                    Toast.makeText(context, "This feature will be available soon.", Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(context, "You must have to login to share post.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        ImageView bookmark = (ImageView) view.findViewById(R.id.bookmark);

        if(data.get(position).getBookmark().equals("true")){
            bookmark.setImageResource(R.drawable.bookmarked);

            bookmark.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which){
                                case DialogInterface.BUTTON_POSITIVE:
                                    //Yes button clicked
                                    mAPIService = ApiUtils.getAPIService();
                                    mAPIService.unbookmark(UserStatus.getUID(),data.get(position).getArticleId()).enqueue(new retrofit2.Callback<Integer>() {
                                        @Override
                                        public void onResponse(Call<Integer> call, retrofit2.Response<Integer> response) {
                                            Log.d(TAG, "post response");
                                            Log.d(TAG, response.code() + "");
                                            if (response.isSuccessful()) {
                                                Log.d(TAG,response.body()+"");

                                                if(response.body() == 1) {
                                                    data.get(position).setBookmark("false");

                                                    if(fragment.equals("bookmark")){
                                                        data.remove(position);
                                                    }

                                                    notifyDataSetChanged();
                                                    Toast.makeText(context,"Article has been removed from bookmarked successfully.",Toast.LENGTH_SHORT).show();
                                                }
                                                else if(response.body() == 0){
                                                    Toast.makeText(context,"Article has not been removed from bookmarked.",Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        }
                                        @Override
                                        public void onFailure(Call<Integer> call, Throwable t) {
                                            Log.e(TAG, "Unable to submit post to API. "+t.getMessage());
                                        }
                                    });
                                    break;

                                case DialogInterface.BUTTON_NEGATIVE:
                                    //No button clicked
                                    break;
                            }
                        }
                    };

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage("Are you sure! you want to unbookmark article?").setPositiveButton("Yes", dialogClickListener)
                            .setNegativeButton("No", dialogClickListener).show();

                }
            });
        }
        else{
            bookmark.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(UserStatus.isLoggedIn()){
                        mAPIService = ApiUtils.getAPIService();
                        mAPIService.bookmark(UserStatus.getUID(),data.get(position).getArticleId()).enqueue(new retrofit2.Callback<Integer>() {
                            @Override
                            public void onResponse(Call<Integer> call, retrofit2.Response<Integer> response) {
                                Log.d(TAG, "post response");
                                Log.d(TAG, response.code() + "");
                                if (response.isSuccessful()) {
                                    Log.d(TAG,response.body()+"");

                                    if(response.body() == 1) {
                                        data.get(position).setBookmark("true");
                                        notifyDataSetChanged();
                                        Toast.makeText(context,"Article has been bookmarked successfully.",Toast.LENGTH_SHORT).show();
                                    }
                                    else if(response.body() == 0){
                                        Toast.makeText(context,"Article has not been bookmarked.",Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                            @Override
                            public void onFailure(Call<Integer> call, Throwable t) {
                                Log.e(TAG, "Unable to submit post to API. "+t.getMessage());
                            }
                        });
                    }
                    else{
                        Toast.makeText(context, "You must have to login to bookmark post.", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }


        LinearLayout comment_sec = (LinearLayout) view.findViewById(R.id.comment_sec);
        comment_sec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(UserStatus.isLoggedIn()){
                    Intent intent = new Intent(context,NewsDetailActivity.class);
                    intent.putExtra("article_id",data.get(position).getArticleId());
                    context.startActivity(intent);
                    activity.finish();
                }
                else{
                    Toast.makeText(context, "You must have to login to post comment.", Toast.LENGTH_SHORT).show();
                }
            }
        });



        return view;
    }
}
