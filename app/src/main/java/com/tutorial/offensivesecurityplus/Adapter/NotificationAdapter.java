package com.tutorial.offensivesecurityplus.Adapter;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tutorial.offensivesecurityplus.Activity.NewsDetailActivity;
import com.tutorial.offensivesecurityplus.AsyncTask.ImageDownloaderTask;
import com.tutorial.offensivesecurityplus.R;
import com.tutorial.offensivesecurityplus.Retrofit.APIService;
import com.tutorial.offensivesecurityplus.Retrofit.ApiUtils;
import com.tutorial.offensivesecurityplus.Retrofit.NewsThumbnail;
import com.tutorial.offensivesecurityplus.Retrofit.Notification;
import com.tutorial.offensivesecurityplus.util.UserStatus;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;

public class NotificationAdapter extends BaseAdapter {
    List<Notification> data;
    Context context;
    Activity activity;

    private APIService mAPIService;
    private String TAG = NotificationAdapter.class.getSimpleName();

    public NotificationAdapter(Context context, Activity activity, List<Notification> data){
        this.context = context;
        this.activity = activity;
        this.data = data;
    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private String calculateTime(String sql_date){

        //Create a date formatter.
        SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String parseTime = sql_date;
        Date parseDate = null;
        try {
            parseDate = formatter.parse(parseTime);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        Calendar sqlDate = Calendar.getInstance();
        sqlDate.setTime(parseDate);

//        Log.d("Current",sqlDate.get(Calendar.HOUR)+"");
//        Log.d("Current",sqlDate.get(Calendar.MINUTE)+"");
//        Log.d("Current",sqlDate.get(Calendar.SECOND)+"");
//        Log.d("Current",sqlDate.get(Calendar.DATE)+"");
//        Log.d("Current",sqlDate.get(Calendar.MONTH)+"");
//        Log.d("Current",sqlDate.get(Calendar.YEAR)+"");

        Calendar curDate = Calendar.getInstance();
        curDate.setTime(new Date());

//        Log.d("Current",curDate.get(Calendar.HOUR)+"");
//        Log.d("Current",curDate.get(Calendar.MINUTE)+"");
//        Log.d("Current",curDate.get(Calendar.SECOND)+"");
//        Log.d("Current",curDate.get(Calendar.DATE)+"");
//        Log.d("Current",curDate.get(Calendar.MONTH)+"");
//        Log.d("Current",curDate.get(Calendar.YEAR)+"");


        long diffInMillisec = curDate.getTimeInMillis() - sqlDate.getTimeInMillis();

//        long diffInMillisec = sqlDate.getTime() - curr_date.getTime();
        long diffInSec = TimeUnit.MILLISECONDS.toSeconds(diffInMillisec);
        Long seconds = diffInSec % 60;
        diffInSec /= 60;
        Long minutes =diffInSec % 60;
        diffInSec /= 60;
        Long hours = diffInSec % 24;
        diffInSec /= 24;
        Long days = diffInSec;


        if(days > 0)
            return days+"d";
        else if(hours > 0)
            return hours+"h "+minutes+"m";
        else if(minutes > 0)
            return minutes+"m";
        else
            return seconds+"s";
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.notification_item,parent,false);

        ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
        if (imageView != null) {
            new ImageDownloaderTask(imageView).execute(data.get(position).getImage());
        }

        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(data.get(position).getTitle());

        TextView description = (TextView) view.findViewById(R.id.description);
        description.setText(data.get(position).getDescription());

        TextView timestamp = (TextView) view.findViewById(R.id.timestamp);
        timestamp.setText("Posted "+calculateTime(data.get(position).getDatestamp())+" ago");


        return view;
    }
}
