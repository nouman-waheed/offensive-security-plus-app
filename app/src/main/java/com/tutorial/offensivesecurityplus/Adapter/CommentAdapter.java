package com.tutorial.offensivesecurityplus.Adapter;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.tutorial.offensivesecurityplus.Activity.NewsDetailActivity;
import com.tutorial.offensivesecurityplus.AsyncTask.ImageDownloaderTask;
import com.tutorial.offensivesecurityplus.R;
import com.tutorial.offensivesecurityplus.Retrofit.APIService;
import com.tutorial.offensivesecurityplus.Retrofit.ApiUtils;
import com.tutorial.offensivesecurityplus.Retrofit.Comment;
import com.tutorial.offensivesecurityplus.util.UserStatus;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;

public class CommentAdapter extends BaseAdapter {
    List<Comment> data;
    Context context;
    Activity activity;
    EditText comment_box;
    ListView listView;

    private APIService mAPIService;
    private String TAG = NewsDetailActivity.class.getSimpleName();

    public CommentAdapter(Context context, List<Comment> data){
        this.context = context;
        this.data = data;
    }

    public CommentAdapter(Context context, Activity activity, EditText comment_box, ListView listView, List<Comment> data){
        this.context = context;
        this.activity = activity;
        this.data = data;
        this.comment_box = comment_box;
        this.listView = listView;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private String calculateTime(String sql_date){

        //Create a date formatter.
        SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String parseTime = sql_date;
        Date parseDate = null;
        try {
            parseDate = formatter.parse(parseTime);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        Calendar sqlDate = Calendar.getInstance();
        sqlDate.setTime(parseDate);

//        Log.d("Current",sqlDate.get(Calendar.HOUR)+"");
//        Log.d("Current",sqlDate.get(Calendar.MINUTE)+"");
//        Log.d("Current",sqlDate.get(Calendar.SECOND)+"");
//        Log.d("Current",sqlDate.get(Calendar.DATE)+"");
//        Log.d("Current",sqlDate.get(Calendar.MONTH)+"");
//        Log.d("Current",sqlDate.get(Calendar.YEAR)+"");

        Calendar curDate = Calendar.getInstance();
        curDate.setTime(new Date());

//        Log.d("Current",curDate.get(Calendar.HOUR)+"");
//        Log.d("Current",curDate.get(Calendar.MINUTE)+"");
//        Log.d("Current",curDate.get(Calendar.SECOND)+"");
//        Log.d("Current",curDate.get(Calendar.DATE)+"");
//        Log.d("Current",curDate.get(Calendar.MONTH)+"");
//        Log.d("Current",curDate.get(Calendar.YEAR)+"");


        long diffInMillisec = curDate.getTimeInMillis() - sqlDate.getTimeInMillis();

//        long diffInMillisec = sqlDate.getTime() - curr_date.getTime();
        long diffInSec = TimeUnit.MILLISECONDS.toSeconds(diffInMillisec);
        Long seconds = diffInSec % 60;
        diffInSec /= 60;
        Long minutes =diffInSec % 60;
        diffInSec /= 60;
        Long hours = diffInSec % 24;
        diffInSec /= 24;
        Long days = diffInSec;


        if(days > 0)
            return days+"d";
        else if(hours > 0)
            return hours+"h "+minutes+"m";
        else if(minutes > 0)
            return minutes+"m";
        else
            return seconds+"s";
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.comment_view,parent,false);

        ImageView avatar = (ImageView) view.findViewById(R.id.avatar);
        if (avatar != null) {
            new ImageDownloaderTask(avatar).execute(data.get(position).getImage());
        }

        TextView name = (TextView) view.findViewById(R.id.username);
        name.setText(data.get(position).getName());

        TextView timestamp = (TextView) view.findViewById(R.id.timestamp);
        if(data.get(position).getDatestamp().equals("few seconds ago"))
            timestamp.setText(data.get(position).getDatestamp());
        else
        timestamp.setText(calculateTime(data.get(position).getDatestamp())+" ago");

        TextView comment = (TextView) view.findViewById(R.id.comment);

        if(data.get(position).getReply().charAt(0) != '@') {
            comment.setText(data.get(position).getReply());
        }
        else{
            String tag = data.get(position).getReply().substring(0,data.get(position).getReply().indexOf(" "));
            String reply = data.get(position).getReply().substring(data.get(position).getReply().indexOf(" "),data.get(position).getReply().length());
            String text = "<font color=#559dab>"+tag+"</font><font color=#96a6b3>"+reply+"</font>";
            comment.setText(Html.fromHtml(text));

        }



        ImageView like = (ImageView) view.findViewById(R.id.like);
        ImageView dislike = (ImageView) view.findViewById(R.id.dislike);
        ImageView reply = (ImageView) view.findViewById(R.id.reply);
        TextView likes_count = (TextView) view.findViewById(R.id.likes_count);
        TextView dislikes_count = (TextView) view.findViewById(R.id.dislikes_count);
        TextView edit_comment = (TextView) view.findViewById(R.id.edit_comment);
        TextView delete_comment = (TextView) view.findViewById(R.id.delete_comment);

        likes_count.setText(data.get(position).getLikes());
        dislikes_count.setText(data.get(position).getDislikes());

        if(UserStatus.getUID().equals(data.get(position).getUid())){

            like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "You cannot like your own comment.", Toast.LENGTH_SHORT).show();
                }
            });

            dislike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "You cannot dislike your own comment.", Toast.LENGTH_SHORT).show();
                }
            });

            reply.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "You cannot reply to your own comment.", Toast.LENGTH_SHORT).show();
                }
            });

            edit_comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    AlertDialog.Builder alert = new AlertDialog.Builder(context);
                    final EditText edittext = new EditText(context);

                    LinearLayout linearLayout = new LinearLayout(context);

                    edittext.setText(data.get(position).getReply());

                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    layoutParams.gravity = Gravity.CENTER;

                    edittext.setLayoutParams(layoutParams);

                    linearLayout.addView(edittext);
                    linearLayout.setPadding(60, 0, 60, 0);



                    alert.setMessage("Edit Your Comment");
//                    alert.setTitle("Edit Your Comment");

                    alert.setView(linearLayout);

                    alert.setPositiveButton("Edit", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            //What ever you want to do with the value
                            final String reply = edittext.getText().toString();

                            mAPIService = ApiUtils.getAPIService();
                            mAPIService.editComment(data.get(position).getCommentId(),reply).enqueue(new retrofit2.Callback<Integer>() {
                                @Override
                                public void onResponse(Call<Integer> call, retrofit2.Response<Integer> response) {
                                    Log.d(TAG, "post response");
                                    Log.d(TAG, response.code() + "");
                                    if (response.isSuccessful()) {
                                        Log.d(TAG,response.body()+"");

                                        if(response.body() == 1) {
                                            data.get(position).setReply(reply);
                                            notifyDataSetChanged();
//                                            NewsDetailActivity.setListViewHeightBasedOnChildren(listView);

                                            Toast.makeText(context,"Comment has been updated successfully.",Toast.LENGTH_SHORT).show();
                                        }
                                        else if(response.body() == 0){
                                            Toast.makeText(context,"Comment has not been updated.",Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }
                                @Override
                                public void onFailure(Call<Integer> call, Throwable t) {
                                    Log.e(TAG, "Unable to submit post to API. "+t.getMessage());
                                }
                            });
                        }
                    });

                    alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            // what ever you want to do with No option.
                        }
                    });

                    alert.show();
                }
            });

            delete_comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which){
                                case DialogInterface.BUTTON_POSITIVE:
                                    //Yes button clicked

                                    mAPIService = ApiUtils.getAPIService();
                                    mAPIService.deleteComment(data.get(position).getCommentId()).enqueue(new retrofit2.Callback<Integer>() {
                                        @Override
                                        public void onResponse(Call<Integer> call, retrofit2.Response<Integer> response) {
                                            Log.d(TAG, "post response");
                                            Log.d(TAG, response.code() + "");
                                            if (response.isSuccessful()) {
                                                Log.d(TAG,response.body()+"");

                                                if(response.body() == 1) {
                                                    data.remove(position);
                                                    notifyDataSetChanged();
//                                                    NewsDetailActivity.setListViewHeightBasedOnChildren(listView);

                                                    Toast.makeText(context,"Comment has been deleted successfully.",Toast.LENGTH_SHORT).show();
                                                }
                                                else if(response.body() == 0){
                                                    Toast.makeText(context,"Comment has not been deleted.",Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        }
                                        @Override
                                        public void onFailure(Call<Integer> call, Throwable t) {
                                            Log.e(TAG, "Unable to submit post to API. "+t.getMessage());
                                        }
                                    });

                                    break;

                                case DialogInterface.BUTTON_NEGATIVE:
                                    //No button clicked
                                    break;
                            }
                        }
                    };

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage("Are you sure! you want to delete comment?").setPositiveButton("Yes", dialogClickListener)
                            .setNegativeButton("No", dialogClickListener).show();
                }
            });



        }
        else{
            edit_comment.setVisibility(View.GONE);
            delete_comment.setVisibility(View.GONE);

            if(data.get(position).getReaction().equals("like")){
                like.setImageResource(R.drawable.liked);

                dislike.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mAPIService = ApiUtils.getAPIService();
                        mAPIService.unlikeDislikeComment(UserStatus.getUID(),data.get(position).getCommentId()).enqueue(new retrofit2.Callback<Integer>() {
                            @Override
                            public void onResponse(Call<Integer> call, retrofit2.Response<Integer> response) {
                                Log.d(TAG, "post response");
                                Log.d(TAG, response.code() + "");
                                if (response.isSuccessful()) {
                                    Log.d(TAG,response.body()+"");

                                    if(response.body() == 1) {
                                        data.get(position).setDislikes(String.valueOf(Integer.parseInt(data.get(position).getDislikes())+1));
                                        data.get(position).setLikes(String.valueOf(Integer.parseInt(data.get(position).getLikes()) - 1));
                                        data.get(position).setReaction("dislike");
                                        notifyDataSetChanged();
//                                            NewsDetailActivity.setListViewHeightBasedOnChildren(listView);

                                        Toast.makeText(context,"Comment has been disliked successfully.",Toast.LENGTH_SHORT).show();
                                    }
                                    else if(response.body() == 0){
                                        Toast.makeText(context,"Comment has not been disliked.",Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                            @Override
                            public void onFailure(Call<Integer> call, Throwable t) {
                                Log.e(TAG, "Unable to submit post to API. "+t.getMessage());
                            }
                        });
                    }
                });

                like.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mAPIService = ApiUtils.getAPIService();
                        mAPIService.unlikeComment(UserStatus.getUID(), data.get(position).getCommentId()).enqueue(new retrofit2.Callback<Integer>() {
                            @Override
                            public void onResponse(Call<Integer> call, retrofit2.Response<Integer> response) {
                                Log.d(TAG, "post response");
                                Log.d(TAG, response.code() + "");
                                if (response.isSuccessful()) {
                                    Log.d(TAG, response.body() + "");

                                    if (response.body() == 1) {
                                        data.get(position).setLikes(String.valueOf(Integer.parseInt(data.get(position).getLikes()) - 1));
                                        data.get(position).setReaction("none");
                                        notifyDataSetChanged();
//                                        NewsDetailActivity.setListViewHeightBasedOnChildren(listView);

                                        Toast.makeText(context, "Comment like has been removed successfully.", Toast.LENGTH_SHORT).show();
                                    } else if (response.body() == 0) {
                                        Toast.makeText(context, "Comment like has not been removed.", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<Integer> call, Throwable t) {
                                Log.e(TAG, "Unable to submit post to API. " + t.getMessage());
                            }
                        });
                    }
                });
            }
            else {
                if(data.get(position).getReaction().equals("dislike")){
                    like.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mAPIService = ApiUtils.getAPIService();
                            mAPIService.undislikeLikeComment(UserStatus.getUID(), data.get(position).getCommentId()).enqueue(new retrofit2.Callback<Integer>() {
                                @Override
                                public void onResponse(Call<Integer> call, retrofit2.Response<Integer> response) {
                                    Log.d(TAG, "post response");
                                    Log.d(TAG, response.code() + "");
                                    if (response.isSuccessful()) {
                                        Log.d(TAG, response.body() + "");

                                        if (response.body() == 1) {
                                            data.get(position).setDislikes(String.valueOf(Integer.parseInt(data.get(position).getDislikes()) - 1));
                                            data.get(position).setLikes(String.valueOf(Integer.parseInt(data.get(position).getLikes()) + 1));
                                            data.get(position).setReaction("like");
                                            notifyDataSetChanged();
//                                        NewsDetailActivity.setListViewHeightBasedOnChildren(listView);

                                            Toast.makeText(context, "Comment has been liked successfully.", Toast.LENGTH_SHORT).show();
                                        } else if (response.body() == 0) {
                                            Toast.makeText(context, "Comment has not been liked.", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Call<Integer> call, Throwable t) {
                                    Log.e(TAG, "Unable to submit post to API. " + t.getMessage());
                                }
                            });
                        }
                    });
                }
                else{
                    like.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mAPIService = ApiUtils.getAPIService();
                            mAPIService.likeComment(UserStatus.getUID(), data.get(position).getCommentId()).enqueue(new retrofit2.Callback<Integer>() {
                                @Override
                                public void onResponse(Call<Integer> call, retrofit2.Response<Integer> response) {
                                    Log.d(TAG, "post response");
                                    Log.d(TAG, response.code() + "");
                                    if (response.isSuccessful()) {
                                        Log.d(TAG, response.body() + "");

                                        if (response.body() == 1) {
                                            data.get(position).setLikes(String.valueOf(Integer.parseInt(data.get(position).getLikes()) + 1));
                                            data.get(position).setReaction("like");
                                            notifyDataSetChanged();
//                                            NewsDetailActivity.setListViewHeightBasedOnChildren(listView);

                                            Toast.makeText(context, "Comment has been liked successfully.", Toast.LENGTH_SHORT).show();
                                        } else if (response.body() == 0) {
                                            Toast.makeText(context, "Comment has not been liked.", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Call<Integer> call, Throwable t) {
                                    Log.e(TAG, "Unable to submit post to API. " + t.getMessage());
                                }
                            });
                        }
                    });
                }
            }

            if(data.get(position).getReaction().equals("dislike")){
                dislike.setImageResource(R.drawable.disliked);

                like.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mAPIService = ApiUtils.getAPIService();
                        mAPIService.undislikeLikeComment(UserStatus.getUID(), data.get(position).getCommentId()).enqueue(new retrofit2.Callback<Integer>() {
                            @Override
                            public void onResponse(Call<Integer> call, retrofit2.Response<Integer> response) {
                                Log.d(TAG, "post response");
                                Log.d(TAG, response.code() + "");
                                if (response.isSuccessful()) {
                                    Log.d(TAG, response.body() + "");

                                    if (response.body() == 1) {
                                        data.get(position).setDislikes(String.valueOf(Integer.parseInt(data.get(position).getDislikes()) - 1));
                                        data.get(position).setLikes(String.valueOf(Integer.parseInt(data.get(position).getLikes()) + 1));
                                        data.get(position).setReaction("like");
                                        notifyDataSetChanged();
//                                        NewsDetailActivity.setListViewHeightBasedOnChildren(listView);

                                        Toast.makeText(context, "Comment has been liked successfully.", Toast.LENGTH_SHORT).show();
                                    } else if (response.body() == 0) {
                                        Toast.makeText(context, "Comment has not been liked.", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<Integer> call, Throwable t) {
                                Log.e(TAG, "Unable to submit post to API. " + t.getMessage());
                            }
                        });
                    }
                });

                dislike.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mAPIService = ApiUtils.getAPIService();
                        mAPIService.undislikeComment(UserStatus.getUID(), data.get(position).getCommentId()).enqueue(new retrofit2.Callback<Integer>() {
                            @Override
                            public void onResponse(Call<Integer> call, retrofit2.Response<Integer> response) {
                                Log.d(TAG, "post response");
                                Log.d(TAG, response.code() + "");
                                if (response.isSuccessful()) {
                                    Log.d(TAG, response.body() + "");

                                    if (response.body() == 1) {
                                        data.get(position).setDislikes(String.valueOf(Integer.parseInt(data.get(position).getDislikes()) - 1));
                                        data.get(position).setReaction("none");
                                        notifyDataSetChanged();
//                                        NewsDetailActivity.setListViewHeightBasedOnChildren(listView);

                                        Toast.makeText(context, "Comment dislike has been removed successfully.", Toast.LENGTH_SHORT).show();
                                    } else if (response.body() == 0) {
                                        Toast.makeText(context, "Comment dislike has not been removed.", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<Integer> call, Throwable t) {
                                Log.e(TAG, "Unable to submit post to API. " + t.getMessage());
                            }
                        });
                    }
                });
            }
            else{
                if(data.get(position).getReaction().equals("like")){
                    dislike.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mAPIService = ApiUtils.getAPIService();
                            mAPIService.unlikeDislikeComment(UserStatus.getUID(),data.get(position).getCommentId()).enqueue(new retrofit2.Callback<Integer>() {
                                @Override
                                public void onResponse(Call<Integer> call, retrofit2.Response<Integer> response) {
                                    Log.d(TAG, "post response");
                                    Log.d(TAG, response.code() + "");
                                    if (response.isSuccessful()) {
                                        Log.d(TAG,response.body()+"");

                                        if(response.body() == 1) {
                                            data.get(position).setDislikes(String.valueOf(Integer.parseInt(data.get(position).getDislikes())+1));
                                            data.get(position).setLikes(String.valueOf(Integer.parseInt(data.get(position).getLikes()) - 1));
                                            data.get(position).setReaction("dislike");
                                            notifyDataSetChanged();
//                                            NewsDetailActivity.setListViewHeightBasedOnChildren(listView);

                                            Toast.makeText(context,"Comment has been disliked successfully.",Toast.LENGTH_SHORT).show();
                                        }
                                        else if(response.body() == 0){
                                            Toast.makeText(context,"Comment has not been disliked.",Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }
                                @Override
                                public void onFailure(Call<Integer> call, Throwable t) {
                                    Log.e(TAG, "Unable to submit post to API. "+t.getMessage());
                                }
                            });
                        }
                    });
                }
                else{
                    dislike.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mAPIService = ApiUtils.getAPIService();
                            mAPIService.dislikeComment(UserStatus.getUID(),data.get(position).getCommentId()).enqueue(new retrofit2.Callback<Integer>() {
                                @Override
                                public void onResponse(Call<Integer> call, retrofit2.Response<Integer> response) {
                                    Log.d(TAG, "post response");
                                    Log.d(TAG, response.code() + "");
                                    if (response.isSuccessful()) {
                                        Log.d(TAG,response.body()+"");

                                        if(response.body() == 1) {
                                            data.get(position).setDislikes(String.valueOf(Integer.parseInt(data.get(position).getDislikes())+1));
                                            data.get(position).setReaction("dislike");
                                            notifyDataSetChanged();
//                                            NewsDetailActivity.setListViewHeightBasedOnChildren(listView);

                                            Toast.makeText(context,"Comment has been disliked successfully.",Toast.LENGTH_SHORT).show();
                                        }
                                        else if(response.body() == 0){
                                            Toast.makeText(context,"Comment has not been disliked.",Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }
                                @Override
                                public void onFailure(Call<Integer> call, Throwable t) {
                                    Log.e(TAG, "Unable to submit post to API. "+t.getMessage());
                                }
                            });
                        }
                    });
                }
            }

            reply.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    NewsDetailActivity.notify_uid = data.get(position).getUid();
                    if(comment_box.requestFocus()) {
                        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(comment_box, InputMethodManager.SHOW_IMPLICIT);
                        comment_box.setText("@"+data.get(position).getName().replace(" ","")+" ");
                        comment_box.setSelection(comment_box.getText().length());
                    }
                }
            });
        }

        return view;
    }
}
